<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreChatTestRequest;
use App\Http\Requests\UpdateChatTestRequest;
use App\Http\Resources\Admin\ChatTestResource;
use App\Model\Meeting\ChatTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ChatTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('chat_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ChatTestResource(ChatTest::all());

    }

    public function store(StoreChatTestRequest $request)
    {
        $chatTest = ChatTest::create($request->all());

        return (new ChatTestResource($chatTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(ChatTest $chatTest)
    {
        abort_if(Gate::denies('chat_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ChatTestResource($chatTest);

    }

    public function update(UpdateChatTestRequest $request, ChatTest $chatTest)
    {
        $chatTest->update($request->all());

        return (new ChatTestResource($chatTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(ChatTest $chatTest)
    {
        abort_if(Gate::denies('chat_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $chatTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
