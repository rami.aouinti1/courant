<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBureauTestRequest;
use App\Http\Requests\UpdateBureauTestRequest;
use App\Http\Resources\Admin\BureauTestResource;
use App\Model\Department\BureauTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BureauTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('bureau_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BureauTestResource(BureauTest::all());

    }

    public function store(StoreBureauTestRequest $request)
    {
        $bureauTest = BureauTest::create($request->all());

        return (new BureauTestResource($bureauTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(BureauTest $bureauTest)
    {
        abort_if(Gate::denies('bureau_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BureauTestResource($bureauTest);

    }

    public function update(UpdateBureauTestRequest $request, BureauTest $bureauTest)
    {
        $bureauTest->update($request->all());

        return (new BureauTestResource($bureauTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(BureauTest $bureauTest)
    {
        abort_if(Gate::denies('bureau_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bureauTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
