<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStatisticesTestRequest;
use App\Http\Requests\UpdateStatisticesTestRequest;
use App\Http\Resources\Admin\StatisticesTestResource;
use App\Model\Statistic\StatisticesTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class StatisticesTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('statistices_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new StatisticesTestResource(StatisticesTest::all());

    }

    public function store(StoreStatisticesTestRequest $request)
    {
        $statisticesTest = StatisticesTest::create($request->all());

        return (new StatisticesTestResource($statisticesTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(StatisticesTest $statisticesTest)
    {
        abort_if(Gate::denies('statistices_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new StatisticesTestResource($statisticesTest);

    }

    public function update(UpdateStatisticesTestRequest $request, StatisticesTest $statisticesTest)
    {
        $statisticesTest->update($request->all());

        return (new StatisticesTestResource($statisticesTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(StatisticesTest $statisticesTest)
    {
        abort_if(Gate::denies('statistices_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $statisticesTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
