<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRapportTestRequest;
use App\Http\Requests\UpdateRapportTestRequest;
use App\Http\Resources\Admin\RapportTestResource;
use App\Model\File\RapportTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RapportTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('rapport_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RapportTestResource(RapportTest::all());

    }

    public function store(StoreRapportTestRequest $request)
    {
        $rapportTest = RapportTest::create($request->all());

        return (new RapportTestResource($rapportTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(RapportTest $rapportTest)
    {
        abort_if(Gate::denies('rapport_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RapportTestResource($rapportTest);

    }

    public function update(UpdateRapportTestRequest $request, RapportTest $rapportTest)
    {
        $rapportTest->update($request->all());

        return (new RapportTestResource($rapportTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(RapportTest $rapportTest)
    {
        abort_if(Gate::denies('rapport_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapportTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
