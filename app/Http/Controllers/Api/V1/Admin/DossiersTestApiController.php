<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDossiersTestRequest;
use App\Http\Requests\UpdateDossiersTestRequest;
use App\Http\Resources\Admin\DossiersTestResource;
use App\Model\File\DossiersTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DossiersTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('dossiers_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DossiersTestResource(DossiersTest::all());

    }

    public function store(StoreDossiersTestRequest $request)
    {
        $dossiersTest = DossiersTest::create($request->all());

        return (new DossiersTestResource($dossiersTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(DossiersTest $dossiersTest)
    {
        abort_if(Gate::denies('dossiers_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DossiersTestResource($dossiersTest);

    }

    public function update(UpdateDossiersTestRequest $request, DossiersTest $dossiersTest)
    {
        $dossiersTest->update($request->all());

        return (new DossiersTestResource($dossiersTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(DossiersTest $dossiersTest)
    {
        abort_if(Gate::denies('dossiers_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $dossiersTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
