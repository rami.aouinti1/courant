<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAncienPartiRequest;
use App\Http\Requests\UpdateAncienPartiRequest;
use App\Http\Resources\Admin\AncienPartiResource;
use App\Model\Profile\AncienParti;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AncienPartiApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ancien_parti_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new AncienPartiResource(AncienParti::with(['user'])->get());

    }

    public function store(StoreAncienPartiRequest $request)
    {
        $ancienParti = AncienParti::create($request->all());

        return (new AncienPartiResource($ancienParti))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(AncienParti $ancienParti)
    {
        abort_if(Gate::denies('ancien_parti_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new AncienPartiResource($ancienParti->load(['user']));

    }

    public function update(UpdateAncienPartiRequest $request, AncienParti $ancienParti)
    {
        $ancienParti->update($request->all());

        return (new AncienPartiResource($ancienParti))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(AncienParti $ancienParti)
    {
        abort_if(Gate::denies('ancien_parti_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ancienParti->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
