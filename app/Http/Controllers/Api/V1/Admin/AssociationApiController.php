<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreAssociationRequest;
use App\Http\Requests\UpdateAssociationRequest;
use App\Http\Resources\Admin\AssociationResource;
use App\Model\Profile\Association;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AssociationApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('association_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new AssociationResource(Association::with(['user'])->get());

    }

    public function store(StoreAssociationRequest $request)
    {
        $association = Association::create($request->all());

        if ($request->input('certification', false)) {
            $association->addMedia(storage_path('tmp/uploads/' . $request->input('certification')))->toMediaCollection('certification');
        }

        return (new AssociationResource($association))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Association $association)
    {
        abort_if(Gate::denies('association_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new AssociationResource($association->load(['user']));

    }

    public function update(UpdateAssociationRequest $request, Association $association)
    {
        $association->update($request->all());

        if ($request->input('certification', false)) {
            if (!$association->certification || $request->input('certification') !== $association->certification->file_name) {
                $association->addMedia(storage_path('tmp/uploads/' . $request->input('certification')))->toMediaCollection('certification');
            }

        } elseif ($association->certification) {
            $association->certification->delete();
        }

        return (new AssociationResource($association))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Association $association)
    {
        abort_if(Gate::denies('association_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $association->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
