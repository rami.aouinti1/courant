<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreLaLignePolitiqueRequest;
use App\Http\Requests\UpdateLaLignePolitiqueRequest;
use App\Http\Resources\Admin\LaLignePolitiqueResource;
use App\Model\Library\LaLignePolitique;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LaLignePolitiqueApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('la_ligne_politique_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new LaLignePolitiqueResource(LaLignePolitique::all());

    }

    public function store(StoreLaLignePolitiqueRequest $request)
    {
        $laLignePolitique = LaLignePolitique::create($request->all());

        return (new LaLignePolitiqueResource($laLignePolitique))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(LaLignePolitique $laLignePolitique)
    {
        abort_if(Gate::denies('la_ligne_politique_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new LaLignePolitiqueResource($laLignePolitique);

    }

    public function update(UpdateLaLignePolitiqueRequest $request, LaLignePolitique $laLignePolitique)
    {
        $laLignePolitique->update($request->all());

        return (new LaLignePolitiqueResource($laLignePolitique))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(LaLignePolitique $laLignePolitique)
    {
        abort_if(Gate::denies('la_ligne_politique_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $laLignePolitique->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
