<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBureauxRequest;
use App\Http\Requests\UpdateBureauxRequest;
use App\Http\Resources\Admin\BureauxResource;
use App\Model\Department\Bureaux;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BureauxApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('bureaux_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BureauxResource(Bureaux::with(['members', 'country'])->get());

    }

    public function store(StoreBureauxRequest $request)
    {
        $bureaux = Bureaux::create($request->all());
        $bureaux->members()->sync($request->input('members', []));

        return (new BureauxResource($bureaux))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Bureaux $bureaux)
    {
        abort_if(Gate::denies('bureaux_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BureauxResource($bureaux->load(['members', 'country']));

    }

    public function update(UpdateBureauxRequest $request, Bureaux $bureaux)
    {
        $bureaux->update($request->all());
        $bureaux->members()->sync($request->input('members', []));

        return (new BureauxResource($bureaux))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Bureaux $bureaux)
    {
        abort_if(Gate::denies('bureaux_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bureaux->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
