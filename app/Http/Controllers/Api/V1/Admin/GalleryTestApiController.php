<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreGalleryTestRequest;
use App\Http\Requests\UpdateGalleryTestRequest;
use App\Http\Resources\Admin\GalleryTestResource;
use App\Model\Component\GalleryTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GalleryTestApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('gallery_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new GalleryTestResource(GalleryTest::all());

    }

    public function store(StoreGalleryTestRequest $request)
    {
        $galleryTest = GalleryTest::create($request->all());

        if ($request->input('gallery', false)) {
            $galleryTest->addMedia(storage_path('tmp/uploads/' . $request->input('gallery')))->toMediaCollection('gallery');
        }

        return (new GalleryTestResource($galleryTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(GalleryTest $galleryTest)
    {
        abort_if(Gate::denies('gallery_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new GalleryTestResource($galleryTest);

    }

    public function update(UpdateGalleryTestRequest $request, GalleryTest $galleryTest)
    {
        $galleryTest->update($request->all());

        if ($request->input('gallery', false)) {
            if (!$galleryTest->gallery || $request->input('gallery') !== $galleryTest->gallery->file_name) {
                $galleryTest->addMedia(storage_path('tmp/uploads/' . $request->input('gallery')))->toMediaCollection('gallery');
            }

        } elseif ($galleryTest->gallery) {
            $galleryTest->gallery->delete();
        }

        return (new GalleryTestResource($galleryTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(GalleryTest $galleryTest)
    {
        abort_if(Gate::denies('gallery_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $galleryTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
