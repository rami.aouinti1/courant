<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDocumentationTestRequest;
use App\Http\Requests\UpdateDocumentationTestRequest;
use App\Http\Resources\Admin\DocumentationTestResource;
use App\Model\Library\DocumentationTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DocumentationTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('documentation_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DocumentationTestResource(DocumentationTest::all());

    }

    public function store(StoreDocumentationTestRequest $request)
    {
        $documentationTest = DocumentationTest::create($request->all());

        return (new DocumentationTestResource($documentationTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(DocumentationTest $documentationTest)
    {
        abort_if(Gate::denies('documentation_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DocumentationTestResource($documentationTest);

    }

    public function update(UpdateDocumentationTestRequest $request, DocumentationTest $documentationTest)
    {
        $documentationTest->update($request->all());

        return (new DocumentationTestResource($documentationTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(DocumentationTest $documentationTest)
    {
        abort_if(Gate::denies('documentation_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $documentationTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
