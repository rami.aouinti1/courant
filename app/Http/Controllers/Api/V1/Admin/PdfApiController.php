<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StorePdfRequest;
use App\Http\Requests\UpdatePdfRequest;
use App\Http\Resources\Admin\PdfResource;
use App\Model\File\Pdf;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class PdfApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('pdf_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PdfResource(Pdf::all());

    }

    public function store(StorePdfRequest $request)
    {
        $pdf = Pdf::create($request->all());

        if ($request->input('file', false)) {
            $pdf->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
        }

        return (new PdfResource($pdf))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Pdf $pdf)
    {
        abort_if(Gate::denies('pdf_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PdfResource($pdf);

    }

    public function update(UpdatePdfRequest $request, Pdf $pdf)
    {
        $pdf->update($request->all());

        if ($request->input('file', false)) {
            if (!$pdf->file || $request->input('file') !== $pdf->file->file_name) {
                $pdf->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
            }

        } elseif ($pdf->file) {
            $pdf->file->delete();
        }

        return (new PdfResource($pdf))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Pdf $pdf)
    {
        abort_if(Gate::denies('pdf_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pdf->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
