<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDeputeseventeRequest;
use App\Http\Requests\UpdateDeputeseventeRequest;
use App\Http\Resources\Admin\DeputeseventeResource;
use App\Model\Event\Deputesevente;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DeputeseventesApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('deputesevente_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DeputeseventeResource(Deputesevente::all());

    }

    public function store(StoreDeputeseventeRequest $request)
    {
        $deputesevente = Deputesevente::create($request->all());

        return (new DeputeseventeResource($deputesevente))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Deputesevente $deputesevente)
    {
        abort_if(Gate::denies('deputesevente_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DeputeseventeResource($deputesevente);

    }

    public function update(UpdateDeputeseventeRequest $request, Deputesevente $deputesevente)
    {
        $deputesevente->update($request->all());

        return (new DeputeseventeResource($deputesevente))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Deputesevente $deputesevente)
    {
        abort_if(Gate::denies('deputesevente_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $deputesevente->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
