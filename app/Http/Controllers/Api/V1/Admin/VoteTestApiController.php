<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVoteTestRequest;
use App\Http\Requests\UpdateVoteTestRequest;
use App\Http\Resources\Admin\VoteTestResource;
use App\Model\Vote\VoteTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class VoteTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('vote_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new VoteTestResource(VoteTest::all());

    }

    public function store(StoreVoteTestRequest $request)
    {
        $voteTest = VoteTest::create($request->all());

        return (new VoteTestResource($voteTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(VoteTest $voteTest)
    {
        abort_if(Gate::denies('vote_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new VoteTestResource($voteTest);

    }

    public function update(UpdateVoteTestRequest $request, VoteTest $voteTest)
    {
        $voteTest->update($request->all());

        return (new VoteTestResource($voteTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(VoteTest $voteTest)
    {
        abort_if(Gate::denies('vote_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $voteTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
