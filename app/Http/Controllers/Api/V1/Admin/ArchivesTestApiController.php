<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreArchivesTestRequest;
use App\Http\Requests\UpdateArchivesTestRequest;
use App\Http\Resources\Admin\ArchivesTestResource;
use App\Model\File\ArchivesTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ArchivesTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('archives_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ArchivesTestResource(ArchivesTest::all());

    }

    public function store(StoreArchivesTestRequest $request)
    {
        $archivesTest = ArchivesTest::create($request->all());

        return (new ArchivesTestResource($archivesTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(ArchivesTest $archivesTest)
    {
        abort_if(Gate::denies('archives_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ArchivesTestResource($archivesTest);

    }

    public function update(UpdateArchivesTestRequest $request, ArchivesTest $archivesTest)
    {
        $archivesTest->update($request->all());

        return (new ArchivesTestResource($archivesTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(ArchivesTest $archivesTest)
    {
        abort_if(Gate::denies('archives_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $archivesTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
