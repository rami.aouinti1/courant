<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMemberTestRequest;
use App\Http\Requests\UpdateMemberTestRequest;
use App\Http\Resources\Admin\MemberTestResource;
use App\Model\Profile\MemberTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MemberTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('member_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MemberTestResource(MemberTest::all());

    }

    public function store(StoreMemberTestRequest $request)
    {
        $memberTest = MemberTest::create($request->all());

        return (new MemberTestResource($memberTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(MemberTest $memberTest)
    {
        abort_if(Gate::denies('member_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new MemberTestResource($memberTest);

    }

    public function update(UpdateMemberTestRequest $request, MemberTest $memberTest)
    {
        $memberTest->update($request->all());

        return (new MemberTestResource($memberTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(MemberTest $memberTest)
    {
        abort_if(Gate::denies('member_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $memberTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
