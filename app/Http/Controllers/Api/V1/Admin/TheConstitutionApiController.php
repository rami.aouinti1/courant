<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreTheConstitutionRequest;
use App\Http\Requests\UpdateTheConstitutionRequest;
use App\Http\Resources\Admin\TheConstitutionResource;
use App\Model\Library\TheConstitution;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TheConstitutionApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('the_constitution_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TheConstitutionResource(TheConstitution::all());

    }

    public function store(StoreTheConstitutionRequest $request)
    {
        $theConstitution = TheConstitution::create($request->all());

        return (new TheConstitutionResource($theConstitution))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(TheConstitution $theConstitution)
    {
        abort_if(Gate::denies('the_constitution_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TheConstitutionResource($theConstitution);

    }

    public function update(UpdateTheConstitutionRequest $request, TheConstitution $theConstitution)
    {
        $theConstitution->update($request->all());

        return (new TheConstitutionResource($theConstitution))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(TheConstitution $theConstitution)
    {
        abort_if(Gate::denies('the_constitution_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $theConstitution->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
