<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreQuizTestRequest;
use App\Http\Requests\UpdateQuizTestRequest;
use App\Http\Resources\Admin\QuizTestResource;
use App\Model\Quiz\QuizTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class QuizTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('quiz_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new QuizTestResource(QuizTest::all());

    }

    public function store(StoreQuizTestRequest $request)
    {
        $quizTest = QuizTest::create($request->all());

        return (new QuizTestResource($quizTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(QuizTest $quizTest)
    {
        abort_if(Gate::denies('quiz_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new QuizTestResource($quizTest);

    }

    public function update(UpdateQuizTestRequest $request, QuizTest $quizTest)
    {
        $quizTest->update($request->all());

        return (new QuizTestResource($quizTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(QuizTest $quizTest)
    {
        abort_if(Gate::denies('quiz_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $quizTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
