<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreConferenceTestRequest;
use App\Http\Requests\UpdateConferenceTestRequest;
use App\Http\Resources\Admin\ConferenceTestResource;
use App\Model\Meeting\ConferenceTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ConferenceTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('conference_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ConferenceTestResource(ConferenceTest::all());

    }

    public function store(StoreConferenceTestRequest $request)
    {
        $conferenceTest = ConferenceTest::create($request->all());

        return (new ConferenceTestResource($conferenceTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(ConferenceTest $conferenceTest)
    {
        abort_if(Gate::denies('conference_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ConferenceTestResource($conferenceTest);

    }

    public function update(UpdateConferenceTestRequest $request, ConferenceTest $conferenceTest)
    {
        $conferenceTest->update($request->all());

        return (new ConferenceTestResource($conferenceTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(ConferenceTest $conferenceTest)
    {
        abort_if(Gate::denies('conference_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $conferenceTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
