<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRapportfinancierTestRequest;
use App\Http\Requests\UpdateRapportfinancierTestRequest;
use App\Http\Resources\Admin\RapportfinancierTestResource;
use App\Model\File\RapportfinancierTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RapportfinancierTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('rapportfinancier_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RapportfinancierTestResource(RapportfinancierTest::all());

    }

    public function store(StoreRapportfinancierTestRequest $request)
    {
        $rapportfinancierTest = RapportfinancierTest::create($request->all());

        return (new RapportfinancierTestResource($rapportfinancierTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(RapportfinancierTest $rapportfinancierTest)
    {
        abort_if(Gate::denies('rapportfinancier_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new RapportfinancierTestResource($rapportfinancierTest);

    }

    public function update(UpdateRapportfinancierTestRequest $request, RapportfinancierTest $rapportfinancierTest)
    {
        $rapportfinancierTest->update($request->all());

        return (new RapportfinancierTestResource($rapportfinancierTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(RapportfinancierTest $rapportfinancierTest)
    {
        abort_if(Gate::denies('rapportfinancier_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapportfinancierTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
