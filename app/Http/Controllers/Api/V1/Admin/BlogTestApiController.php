<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlogTestRequest;
use App\Http\Requests\UpdateBlogTestRequest;
use App\Http\Resources\Admin\BlogTestResource;
use App\Model\Blog\BlogTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BlogTestApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('blog_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BlogTestResource(BlogTest::all());

    }

    public function store(StoreBlogTestRequest $request)
    {
        $blogTest = BlogTest::create($request->all());

        return (new BlogTestResource($blogTest))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(BlogTest $blogTest)
    {
        abort_if(Gate::denies('blog_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new BlogTestResource($blogTest);

    }

    public function update(UpdateBlogTestRequest $request, BlogTest $blogTest)
    {
        $blogTest->update($request->all());

        return (new BlogTestResource($blogTest))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(BlogTest $blogTest)
    {
        abort_if(Gate::denies('blog_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $blogTest->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
