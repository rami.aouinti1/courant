<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreDiplomRequest;
use App\Http\Requests\UpdateDiplomRequest;
use App\Http\Resources\Admin\DiplomResource;
use App\Model\Profile\Diplom;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DiplomApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('diplom_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DiplomResource(Diplom::with(['user'])->get());

    }

    public function store(StoreDiplomRequest $request)
    {
        $diplom = Diplom::create($request->all());

        if ($request->input('files', false)) {
            $diplom->addMedia(storage_path('tmp/uploads/' . $request->input('files')))->toMediaCollection('files');
        }

        return (new DiplomResource($diplom))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);

    }

    public function show(Diplom $diplom)
    {
        abort_if(Gate::denies('diplom_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DiplomResource($diplom->load(['user']));

    }

    public function update(UpdateDiplomRequest $request, Diplom $diplom)
    {
        $diplom->update($request->all());

        if ($request->input('files', false)) {
            if (!$diplom->files || $request->input('files') !== $diplom->files->file_name) {
                $diplom->addMedia(storage_path('tmp/uploads/' . $request->input('files')))->toMediaCollection('files');
            }

        } elseif ($diplom->files) {
            $diplom->files->delete();
        }

        return (new DiplomResource($diplom))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);

    }

    public function destroy(Diplom $diplom)
    {
        abort_if(Gate::denies('diplom_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $diplom->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
