<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyPdfRequest;
use App\Http\Requests\StorePdfRequest;
use App\Http\Requests\UpdatePdfRequest;
use App\Model\File\Pdf;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class PdfController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('pdf_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pdfs = Pdf::all();

        return view('admin.pdfs.index', compact('pdfs'));
    }

    public function create()
    {
        abort_if(Gate::denies('pdf_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pdfs.create');
    }

    public function store(StorePdfRequest $request)
    {
        $pdf = Pdf::create($request->all());

        foreach ($request->input('file', []) as $file) {
            $pdf->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('file');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $pdf->id]);
        }

        return redirect()->route('admin.pdfs.index');

    }

    public function edit(Pdf $pdf)
    {
        abort_if(Gate::denies('pdf_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pdfs.edit', compact('pdf'));
    }

    public function update(UpdatePdfRequest $request, Pdf $pdf)
    {
        $pdf->update($request->all());

        if (count($pdf->file) > 0) {
            foreach ($pdf->file as $media) {
                if (!in_array($media->file_name, $request->input('file', []))) {
                    $media->delete();
                }

            }

        }

        $media = $pdf->file->pluck('file_name')->toArray();

        foreach ($request->input('file', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $pdf->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('file');
            }

        }

        return redirect()->route('admin.pdfs.index');

    }

    public function show(Pdf $pdf)
    {
        abort_if(Gate::denies('pdf_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.pdfs.show', compact('pdf'));
    }

    public function destroy(Pdf $pdf)
    {
        abort_if(Gate::denies('pdf_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $pdf->delete();

        return back();

    }

    public function massDestroy(MassDestroyPdfRequest $request)
    {
        Pdf::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('pdf_create') && Gate::denies('pdf_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Pdf();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);

    }

}
