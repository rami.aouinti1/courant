<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyVoteTestRequest;
use App\Http\Requests\StoreVoteTestRequest;
use App\Http\Requests\UpdateVoteTestRequest;
use App\Model\Vote\VoteTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class VoteTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('vote_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $voteTests = VoteTest::all();

        return view('admin.voteTests.index', compact('voteTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('vote_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.voteTests.create');
    }

    public function store(StoreVoteTestRequest $request)
    {
        $voteTest = VoteTest::create($request->all());

        return redirect()->route('admin.vote-tests.index');

    }

    public function edit(VoteTest $voteTest)
    {
        abort_if(Gate::denies('vote_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.voteTests.edit', compact('voteTest'));
    }

    public function update(UpdateVoteTestRequest $request, VoteTest $voteTest)
    {
        $voteTest->update($request->all());

        return redirect()->route('admin.vote-tests.index');

    }

    public function show(VoteTest $voteTest)
    {
        abort_if(Gate::denies('vote_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.voteTests.show', compact('voteTest'));
    }

    public function destroy(VoteTest $voteTest)
    {
        abort_if(Gate::denies('vote_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $voteTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyVoteTestRequest $request)
    {
        VoteTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
