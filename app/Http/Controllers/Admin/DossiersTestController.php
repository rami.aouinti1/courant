<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyDossiersTestRequest;
use App\Http\Requests\StoreDossiersTestRequest;
use App\Http\Requests\UpdateDossiersTestRequest;
use App\Model\File\DossiersTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DossiersTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('dossiers_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $dossiersTests = DossiersTest::all();

        return view('admin.dossiersTests.index', compact('dossiersTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('dossiers_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.dossiersTests.create');
    }

    public function store(StoreDossiersTestRequest $request)
    {
        $dossiersTest = DossiersTest::create($request->all());

        return redirect()->route('admin.dossiers-tests.index');

    }

    public function edit(DossiersTest $dossiersTest)
    {
        abort_if(Gate::denies('dossiers_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.dossiersTests.edit', compact('dossiersTest'));
    }

    public function update(UpdateDossiersTestRequest $request, DossiersTest $dossiersTest)
    {
        $dossiersTest->update($request->all());

        return redirect()->route('admin.dossiers-tests.index');

    }

    public function show(DossiersTest $dossiersTest)
    {
        abort_if(Gate::denies('dossiers_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.dossiersTests.show', compact('dossiersTest'));
    }

    public function destroy(DossiersTest $dossiersTest)
    {
        abort_if(Gate::denies('dossiers_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $dossiersTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyDossiersTestRequest $request)
    {
        DossiersTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
