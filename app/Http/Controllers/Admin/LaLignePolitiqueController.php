<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyLaLignePolitiqueRequest;
use App\Http\Requests\StoreLaLignePolitiqueRequest;
use App\Http\Requests\UpdateLaLignePolitiqueRequest;
use App\Model\Library\LaLignePolitique;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class LaLignePolitiqueController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('la_ligne_politique_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $laLignePolitiques = LaLignePolitique::all();

        return view('admin.laLignePolitiques.index', compact('laLignePolitiques'));
    }

    public function create()
    {
        abort_if(Gate::denies('la_ligne_politique_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.laLignePolitiques.create');
    }

    public function store(StoreLaLignePolitiqueRequest $request)
    {
        $laLignePolitique = LaLignePolitique::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $laLignePolitique->id]);
        }

        return redirect()->route('admin.la-ligne-politiques.index');

    }

    public function edit(LaLignePolitique $laLignePolitique)
    {
        abort_if(Gate::denies('la_ligne_politique_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.laLignePolitiques.edit', compact('laLignePolitique'));
    }

    public function update(UpdateLaLignePolitiqueRequest $request, LaLignePolitique $laLignePolitique)
    {
        $laLignePolitique->update($request->all());

        return redirect()->route('admin.la-ligne-politiques.index');

    }

    public function show(LaLignePolitique $laLignePolitique)
    {
        abort_if(Gate::denies('la_ligne_politique_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.laLignePolitiques.show', compact('laLignePolitique'));
    }

    public function destroy(LaLignePolitique $laLignePolitique)
    {
        abort_if(Gate::denies('la_ligne_politique_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $laLignePolitique->delete();

        return back();

    }

    public function massDestroy(MassDestroyLaLignePolitiqueRequest $request)
    {
        LaLignePolitique::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('la_ligne_politique_create') && Gate::denies('la_ligne_politique_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new LaLignePolitique();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);

    }

}
