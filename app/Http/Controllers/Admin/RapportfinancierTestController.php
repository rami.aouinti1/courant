<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRapportfinancierTestRequest;
use App\Http\Requests\StoreRapportfinancierTestRequest;
use App\Http\Requests\UpdateRapportfinancierTestRequest;
use App\Model\File\RapportfinancierTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RapportfinancierTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('rapportfinancier_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapportfinancierTests = RapportfinancierTest::all();

        return view('admin.rapportfinancierTests.index', compact('rapportfinancierTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('rapportfinancier_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapportfinancierTests.create');
    }

    public function store(StoreRapportfinancierTestRequest $request)
    {
        $rapportfinancierTest = RapportfinancierTest::create($request->all());

        return redirect()->route('admin.rapportfinancier-tests.index');

    }

    public function edit(RapportfinancierTest $rapportfinancierTest)
    {
        abort_if(Gate::denies('rapportfinancier_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapportfinancierTests.edit', compact('rapportfinancierTest'));
    }

    public function update(UpdateRapportfinancierTestRequest $request, RapportfinancierTest $rapportfinancierTest)
    {
        $rapportfinancierTest->update($request->all());

        return redirect()->route('admin.rapportfinancier-tests.index');

    }

    public function show(RapportfinancierTest $rapportfinancierTest)
    {
        abort_if(Gate::denies('rapportfinancier_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapportfinancierTests.show', compact('rapportfinancierTest'));
    }

    public function destroy(RapportfinancierTest $rapportfinancierTest)
    {
        abort_if(Gate::denies('rapportfinancier_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapportfinancierTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyRapportfinancierTestRequest $request)
    {
        RapportfinancierTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
