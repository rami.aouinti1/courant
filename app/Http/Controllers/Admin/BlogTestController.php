<?php

namespace App\Http\Controllers\Admin;

use App\Model\Blog\BlogTest;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBlogTestRequest;
use App\Http\Requests\StoreBlogTestRequest;
use App\Http\Requests\UpdateBlogTestRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BlogTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('blog_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $blogTests = BlogTest::all();

        return view('admin.blogTests.index', compact('blogTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('blog_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.blogTests.create');
    }

    public function store(StoreBlogTestRequest $request)
    {
        $blogTest = BlogTest::create($request->all());

        return redirect()->route('admin.blog-tests.index');

    }

    public function edit(BlogTest $blogTest)
    {
        abort_if(Gate::denies('blog_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.blogTests.edit', compact('blogTest'));
    }

    public function update(UpdateBlogTestRequest $request, BlogTest $blogTest)
    {
        $blogTest->update($request->all());

        return redirect()->route('admin.blog-tests.index');

    }

    public function show(BlogTest $blogTest)
    {
        abort_if(Gate::denies('blog_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.blogTests.show', compact('blogTest'));
    }

    public function destroy(BlogTest $blogTest)
    {
        abort_if(Gate::denies('blog_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $blogTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyBlogTestRequest $request)
    {
        BlogTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
