<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyGalleryTestRequest;
use App\Http\Requests\StoreGalleryTestRequest;
use App\Http\Requests\UpdateGalleryTestRequest;
use App\Model\Component\GalleryTest;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class GalleryTestController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('gallery_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $galleryTests = GalleryTest::all();

        return view('admin.galleryTests.index', compact('galleryTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('gallery_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.galleryTests.create');
    }

    public function store(StoreGalleryTestRequest $request)
    {
        $galleryTest = GalleryTest::create($request->all());

        foreach ($request->input('gallery', []) as $file) {
            $galleryTest->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('gallery');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $galleryTest->id]);
        }

        return redirect()->route('admin.gallery-tests.index');

    }

    public function edit(GalleryTest $galleryTest)
    {
        abort_if(Gate::denies('gallery_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.galleryTests.edit', compact('galleryTest'));
    }

    public function update(UpdateGalleryTestRequest $request, GalleryTest $galleryTest)
    {
        $galleryTest->update($request->all());

        if (count($galleryTest->gallery) > 0) {
            foreach ($galleryTest->gallery as $media) {
                if (!in_array($media->file_name, $request->input('gallery', []))) {
                    $media->delete();
                }

            }

        }

        $media = $galleryTest->gallery->pluck('file_name')->toArray();

        foreach ($request->input('gallery', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $galleryTest->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('gallery');
            }

        }

        return redirect()->route('admin.gallery-tests.index');

    }

    public function show(GalleryTest $galleryTest)
    {
        abort_if(Gate::denies('gallery_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.galleryTests.show', compact('galleryTest'));
    }

    public function destroy(GalleryTest $galleryTest)
    {
        abort_if(Gate::denies('gallery_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $galleryTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyGalleryTestRequest $request)
    {
        GalleryTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('gallery_test_create') && Gate::denies('gallery_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new GalleryTest();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);

    }

}
