<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMemberTestRequest;
use App\Http\Requests\StoreMemberTestRequest;
use App\Http\Requests\UpdateMemberTestRequest;
use App\Model\Profile\MemberTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MemberTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('member_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $memberTests = MemberTest::all();

        return view('admin.memberTests.index', compact('memberTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('member_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.memberTests.create');
    }

    public function store(StoreMemberTestRequest $request)
    {
        $memberTest = MemberTest::create($request->all());

        return redirect()->route('admin.member-tests.index');

    }

    public function edit(MemberTest $memberTest)
    {
        abort_if(Gate::denies('member_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.memberTests.edit', compact('memberTest'));
    }

    public function update(UpdateMemberTestRequest $request, MemberTest $memberTest)
    {
        $memberTest->update($request->all());

        return redirect()->route('admin.member-tests.index');

    }

    public function show(MemberTest $memberTest)
    {
        abort_if(Gate::denies('member_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.memberTests.show', compact('memberTest'));
    }

    public function destroy(MemberTest $memberTest)
    {
        abort_if(Gate::denies('member_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $memberTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyMemberTestRequest $request)
    {
        MemberTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
