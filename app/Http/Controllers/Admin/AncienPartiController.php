<?php

namespace App\Http\Controllers\Admin;

use App\Model\Profile\AncienParti;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyAncienPartiRequest;
use App\Http\Requests\StoreAncienPartiRequest;
use App\Http\Requests\UpdateAncienPartiRequest;
use App\Model\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AncienPartiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('ancien_parti_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ancienPartis = AncienParti::all();

        return view('admin.ancienPartis.index', compact('ancienPartis'));
    }

    public function create()
    {
        abort_if(Gate::denies('ancien_parti_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.ancienPartis.create', compact('users'));
    }

    public function store(StoreAncienPartiRequest $request)
    {
        $ancienParti = AncienParti::create($request->all());

        return redirect()->route('admin.ancien-partis.index');

    }

    public function edit(AncienParti $ancienParti)
    {
        abort_if(Gate::denies('ancien_parti_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $ancienParti->load('user');

        return view('admin.ancienPartis.edit', compact('users', 'ancienParti'));
    }

    public function update(UpdateAncienPartiRequest $request, AncienParti $ancienParti)
    {
        $ancienParti->update($request->all());

        return redirect()->route('admin.ancien-partis.index');

    }

    public function show(AncienParti $ancienParti)
    {
        abort_if(Gate::denies('ancien_parti_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ancienParti->load('user');

        return view('admin.ancienPartis.show', compact('ancienParti'));
    }

    public function destroy(AncienParti $ancienParti)
    {
        abort_if(Gate::denies('ancien_parti_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ancienParti->delete();

        return back();

    }

    public function massDestroy(MassDestroyAncienPartiRequest $request)
    {
        AncienParti::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
