<?php

namespace App\Http\Controllers\Admin;

use App\Model\Meeting\ChatTest;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyChatTestRequest;
use App\Http\Requests\StoreChatTestRequest;
use App\Http\Requests\UpdateChatTestRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ChatTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('chat_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $chatTests = ChatTest::all();

        return view('admin.chatTests.index', compact('chatTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('chat_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.chatTests.create');
    }

    public function store(StoreChatTestRequest $request)
    {
        $chatTest = ChatTest::create($request->all());

        return redirect()->route('admin.chat-tests.index');

    }

    public function edit(ChatTest $chatTest)
    {
        abort_if(Gate::denies('chat_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.chatTests.edit', compact('chatTest'));
    }

    public function update(UpdateChatTestRequest $request, ChatTest $chatTest)
    {
        $chatTest->update($request->all());

        return redirect()->route('admin.chat-tests.index');

    }

    public function show(ChatTest $chatTest)
    {
        abort_if(Gate::denies('chat_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.chatTests.show', compact('chatTest'));
    }

    public function destroy(ChatTest $chatTest)
    {
        abort_if(Gate::denies('chat_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $chatTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyChatTestRequest $request)
    {
        ChatTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
