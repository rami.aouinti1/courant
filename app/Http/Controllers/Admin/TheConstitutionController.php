<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyTheConstitutionRequest;
use App\Http\Requests\StoreTheConstitutionRequest;
use App\Http\Requests\UpdateTheConstitutionRequest;
use App\Model\Library\TheConstitution;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class TheConstitutionController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('the_constitution_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $theConstitutions = TheConstitution::all();

        return view('admin.theConstitutions.index', compact('theConstitutions'));
    }

    public function create()
    {
        abort_if(Gate::denies('the_constitution_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.theConstitutions.create');
    }

    public function store(StoreTheConstitutionRequest $request)
    {
        $theConstitution = TheConstitution::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $theConstitution->id]);
        }

        return redirect()->route('admin.the-constitutions.index');

    }

    public function edit(TheConstitution $theConstitution)
    {
        abort_if(Gate::denies('the_constitution_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.theConstitutions.edit', compact('theConstitution'));
    }

    public function update(UpdateTheConstitutionRequest $request, TheConstitution $theConstitution)
    {
        $theConstitution->update($request->all());

        return redirect()->route('admin.the-constitutions.index');

    }

    public function show(TheConstitution $theConstitution)
    {
        abort_if(Gate::denies('the_constitution_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.theConstitutions.show', compact('theConstitution'));
    }

    public function destroy(TheConstitution $theConstitution)
    {
        abort_if(Gate::denies('the_constitution_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $theConstitution->delete();

        return back();

    }

    public function massDestroy(MassDestroyTheConstitutionRequest $request)
    {
        TheConstitution::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('the_constitution_create') && Gate::denies('the_constitution_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new TheConstitution();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);

    }

}
