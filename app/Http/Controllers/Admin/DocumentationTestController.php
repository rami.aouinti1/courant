<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyDocumentationTestRequest;
use App\Http\Requests\StoreDocumentationTestRequest;
use App\Http\Requests\UpdateDocumentationTestRequest;
use App\Model\Library\DocumentationTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DocumentationTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('documentation_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $documentationTests = DocumentationTest::all();

        return view('admin.documentationTests.index', compact('documentationTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('documentation_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.documentationTests.create');
    }

    public function store(StoreDocumentationTestRequest $request)
    {
        $documentationTest = DocumentationTest::create($request->all());

        return redirect()->route('admin.documentation-tests.index');

    }

    public function edit(DocumentationTest $documentationTest)
    {
        abort_if(Gate::denies('documentation_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.documentationTests.edit', compact('documentationTest'));
    }

    public function update(UpdateDocumentationTestRequest $request, DocumentationTest $documentationTest)
    {
        $documentationTest->update($request->all());

        return redirect()->route('admin.documentation-tests.index');

    }

    public function show(DocumentationTest $documentationTest)
    {
        abort_if(Gate::denies('documentation_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.documentationTests.show', compact('documentationTest'));
    }

    public function destroy(DocumentationTest $documentationTest)
    {
        abort_if(Gate::denies('documentation_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $documentationTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyDocumentationTestRequest $request)
    {
        DocumentationTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
