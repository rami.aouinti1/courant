<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRapportTestRequest;
use App\Http\Requests\StoreRapportTestRequest;
use App\Http\Requests\UpdateRapportTestRequest;
use App\Model\File\RapportTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RapportTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('rapport_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapportTests = RapportTest::all();

        return view('admin.rapportTests.index', compact('rapportTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('rapport_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapportTests.create');
    }

    public function store(StoreRapportTestRequest $request)
    {
        $rapportTest = RapportTest::create($request->all());

        return redirect()->route('admin.rapport-tests.index');

    }

    public function edit(RapportTest $rapportTest)
    {
        abort_if(Gate::denies('rapport_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapportTests.edit', compact('rapportTest'));
    }

    public function update(UpdateRapportTestRequest $request, RapportTest $rapportTest)
    {
        $rapportTest->update($request->all());

        return redirect()->route('admin.rapport-tests.index');

    }

    public function show(RapportTest $rapportTest)
    {
        abort_if(Gate::denies('rapport_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.rapportTests.show', compact('rapportTest'));
    }

    public function destroy(RapportTest $rapportTest)
    {
        abort_if(Gate::denies('rapport_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $rapportTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyRapportTestRequest $request)
    {
        RapportTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
