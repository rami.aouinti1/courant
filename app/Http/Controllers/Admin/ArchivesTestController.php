<?php

namespace App\Http\Controllers\Admin;

use App\Model\File\ArchivesTest;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyArchivesTestRequest;
use App\Http\Requests\StoreArchivesTestRequest;
use App\Http\Requests\UpdateArchivesTestRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ArchivesTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('archives_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $archivesTests = ArchivesTest::all();

        return view('admin.archivesTests.index', compact('archivesTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('archives_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.archivesTests.create');
    }

    public function store(StoreArchivesTestRequest $request)
    {
        $archivesTest = ArchivesTest::create($request->all());

        return redirect()->route('admin.archives-tests.index');

    }

    public function edit(ArchivesTest $archivesTest)
    {
        abort_if(Gate::denies('archives_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.archivesTests.edit', compact('archivesTest'));
    }

    public function update(UpdateArchivesTestRequest $request, ArchivesTest $archivesTest)
    {
        $archivesTest->update($request->all());

        return redirect()->route('admin.archives-tests.index');

    }

    public function show(ArchivesTest $archivesTest)
    {
        abort_if(Gate::denies('archives_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.archivesTests.show', compact('archivesTest'));
    }

    public function destroy(ArchivesTest $archivesTest)
    {
        abort_if(Gate::denies('archives_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $archivesTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyArchivesTestRequest $request)
    {
        ArchivesTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
