<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyStatisticesTestRequest;
use App\Http\Requests\StoreStatisticesTestRequest;
use App\Http\Requests\UpdateStatisticesTestRequest;
use App\Model\Statistic\StatisticesTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class StatisticesTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('statistices_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $statisticesTests = StatisticesTest::all();

        return view('admin.statisticesTests.index', compact('statisticesTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('statistices_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.statisticesTests.create');
    }

    public function store(StoreStatisticesTestRequest $request)
    {
        $statisticesTest = StatisticesTest::create($request->all());

        return redirect()->route('admin.statistices-tests.index');

    }

    public function edit(StatisticesTest $statisticesTest)
    {
        abort_if(Gate::denies('statistices_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.statisticesTests.edit', compact('statisticesTest'));
    }

    public function update(UpdateStatisticesTestRequest $request, StatisticesTest $statisticesTest)
    {
        $statisticesTest->update($request->all());

        return redirect()->route('admin.statistices-tests.index');

    }

    public function show(StatisticesTest $statisticesTest)
    {
        abort_if(Gate::denies('statistices_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.statisticesTests.show', compact('statisticesTest'));
    }

    public function destroy(StatisticesTest $statisticesTest)
    {
        abort_if(Gate::denies('statistices_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $statisticesTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyStatisticesTestRequest $request)
    {
        StatisticesTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
