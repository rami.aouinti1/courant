<?php

namespace App\Http\Controllers\Admin;

use App\Model\Department\BureauTest;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBureauTestRequest;
use App\Http\Requests\StoreBureauTestRequest;
use App\Http\Requests\UpdateBureauTestRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BureauTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('bureau_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bureauTests = BureauTest::all();

        return view('admin.bureauTests.index', compact('bureauTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('bureau_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bureauTests.create');
    }

    public function store(StoreBureauTestRequest $request)
    {
        $bureauTest = BureauTest::create($request->all());

        return redirect()->route('admin.bureau-tests.index');

    }

    public function edit(BureauTest $bureauTest)
    {
        abort_if(Gate::denies('bureau_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bureauTests.edit', compact('bureauTest'));
    }

    public function update(UpdateBureauTestRequest $request, BureauTest $bureauTest)
    {
        $bureauTest->update($request->all());

        return redirect()->route('admin.bureau-tests.index');

    }

    public function show(BureauTest $bureauTest)
    {
        abort_if(Gate::denies('bureau_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.bureauTests.show', compact('bureauTest'));
    }

    public function destroy(BureauTest $bureauTest)
    {
        abort_if(Gate::denies('bureau_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bureauTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyBureauTestRequest $request)
    {
        BureauTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
