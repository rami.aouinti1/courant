<?php

namespace App\Http\Controllers\Admin;

use App\Model\Event\Deputesevente;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyDeputeseventeRequest;
use App\Http\Requests\StoreDeputeseventeRequest;
use App\Http\Requests\UpdateDeputeseventeRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DeputeseventesController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('deputesevente_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $deputeseventes = Deputesevente::all();

        return view('admin.deputeseventes.index', compact('deputeseventes'));
    }

    public function create()
    {
        abort_if(Gate::denies('deputesevente_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.deputeseventes.create');
    }

    public function store(StoreDeputeseventeRequest $request)
    {
        $deputesevente = Deputesevente::create($request->all());

        return redirect()->route('admin.deputeseventes.index');

    }

    public function edit(Deputesevente $deputesevente)
    {
        abort_if(Gate::denies('deputesevente_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.deputeseventes.edit', compact('deputesevente'));
    }

    public function update(UpdateDeputeseventeRequest $request, Deputesevente $deputesevente)
    {
        $deputesevente->update($request->all());

        return redirect()->route('admin.deputeseventes.index');

    }

    public function show(Deputesevente $deputesevente)
    {
        abort_if(Gate::denies('deputesevente_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.deputeseventes.show', compact('deputesevente'));
    }

    public function destroy(Deputesevente $deputesevente)
    {
        abort_if(Gate::denies('deputesevente_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $deputesevente->delete();

        return back();

    }

    public function massDestroy(MassDestroyDeputeseventeRequest $request)
    {
        Deputesevente::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
