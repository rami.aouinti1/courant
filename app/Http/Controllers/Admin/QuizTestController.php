<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyQuizTestRequest;
use App\Http\Requests\StoreQuizTestRequest;
use App\Http\Requests\UpdateQuizTestRequest;
use App\Model\Quiz\QuizTest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class QuizTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('quiz_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $quizTests = QuizTest::all();

        return view('admin.quizTests.index', compact('quizTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('quiz_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.quizTests.create');
    }

    public function store(StoreQuizTestRequest $request)
    {
        $quizTest = QuizTest::create($request->all());

        return redirect()->route('admin.quiz-tests.index');

    }

    public function edit(QuizTest $quizTest)
    {
        abort_if(Gate::denies('quiz_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.quizTests.edit', compact('quizTest'));
    }

    public function update(UpdateQuizTestRequest $request, QuizTest $quizTest)
    {
        $quizTest->update($request->all());

        return redirect()->route('admin.quiz-tests.index');

    }

    public function show(QuizTest $quizTest)
    {
        abort_if(Gate::denies('quiz_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.quizTests.show', compact('quizTest'));
    }

    public function destroy(QuizTest $quizTest)
    {
        abort_if(Gate::denies('quiz_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $quizTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyQuizTestRequest $request)
    {
        QuizTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
