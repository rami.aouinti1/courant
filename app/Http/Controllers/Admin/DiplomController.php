<?php

namespace App\Http\Controllers\Admin;

use App\Model\Profile\Diplom;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyDiplomRequest;
use App\Http\Requests\StoreDiplomRequest;
use App\Http\Requests\UpdateDiplomRequest;
use App\Model\User;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class DiplomController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('diplom_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $diploms = Diplom::all();

        return view('admin.diploms.index', compact('diploms'));
    }

    public function create()
    {
        abort_if(Gate::denies('diplom_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.diploms.create', compact('users'));
    }

    public function store(StoreDiplomRequest $request)
    {
        $diplom = Diplom::create($request->all());

        foreach ($request->input('files', []) as $file) {
            $diplom->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('files');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $diplom->id]);
        }

        return redirect()->route('admin.diploms.index');

    }

    public function edit(Diplom $diplom)
    {
        abort_if(Gate::denies('diplom_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $diplom->load('user');

        return view('admin.diploms.edit', compact('users', 'diplom'));
    }

    public function update(UpdateDiplomRequest $request, Diplom $diplom)
    {
        $diplom->update($request->all());

        if (count($diplom->files) > 0) {
            foreach ($diplom->files as $media) {
                if (!in_array($media->file_name, $request->input('files', []))) {
                    $media->delete();
                }

            }

        }

        $media = $diplom->files->pluck('file_name')->toArray();

        foreach ($request->input('files', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $diplom->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('files');
            }

        }

        return redirect()->route('admin.diploms.index');

    }

    public function show(Diplom $diplom)
    {
        abort_if(Gate::denies('diplom_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $diplom->load('user');

        return view('admin.diploms.show', compact('diplom'));
    }

    public function destroy(Diplom $diplom)
    {
        abort_if(Gate::denies('diplom_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $diplom->delete();

        return back();

    }

    public function massDestroy(MassDestroyDiplomRequest $request)
    {
        Diplom::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('diplom_create') && Gate::denies('diplom_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Diplom();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);

    }

}
