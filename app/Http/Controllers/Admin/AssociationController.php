<?php

namespace App\Http\Controllers\Admin;

use App\Model\Profile\Association;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyAssociationRequest;
use App\Http\Requests\StoreAssociationRequest;
use App\Http\Requests\UpdateAssociationRequest;
use App\Model\User;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class AssociationController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('association_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $associations = Association::all();

        return view('admin.associations.index', compact('associations'));
    }

    public function create()
    {
        abort_if(Gate::denies('association_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.associations.create', compact('users'));
    }

    public function store(StoreAssociationRequest $request)
    {
        $association = Association::create($request->all());

        foreach ($request->input('certification', []) as $file) {
            $association->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('certification');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $association->id]);
        }

        return redirect()->route('admin.associations.index');

    }

    public function edit(Association $association)
    {
        abort_if(Gate::denies('association_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $association->load('user');

        return view('admin.associations.edit', compact('users', 'association'));
    }

    public function update(UpdateAssociationRequest $request, Association $association)
    {
        $association->update($request->all());

        if (count($association->certification) > 0) {
            foreach ($association->certification as $media) {
                if (!in_array($media->file_name, $request->input('certification', []))) {
                    $media->delete();
                }

            }

        }

        $media = $association->certification->pluck('file_name')->toArray();

        foreach ($request->input('certification', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $association->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('certification');
            }

        }

        return redirect()->route('admin.associations.index');

    }

    public function show(Association $association)
    {
        abort_if(Gate::denies('association_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $association->load('user');

        return view('admin.associations.show', compact('association'));
    }

    public function destroy(Association $association)
    {
        abort_if(Gate::denies('association_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $association->delete();

        return back();

    }

    public function massDestroy(MassDestroyAssociationRequest $request)
    {
        Association::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('association_create') && Gate::denies('association_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Association();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);

    }

}
