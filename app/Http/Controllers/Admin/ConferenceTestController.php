<?php

namespace App\Http\Controllers\Admin;

use App\Model\Meeting\ConferenceTest;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyConferenceTestRequest;
use App\Http\Requests\StoreConferenceTestRequest;
use App\Http\Requests\UpdateConferenceTestRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ConferenceTestController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('conference_test_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $conferenceTests = ConferenceTest::all();

        return view('admin.conferenceTests.index', compact('conferenceTests'));
    }

    public function create()
    {
        abort_if(Gate::denies('conference_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.conferenceTests.create');
    }

    public function store(StoreConferenceTestRequest $request)
    {
        $conferenceTest = ConferenceTest::create($request->all());

        return redirect()->route('admin.conference-tests.index');

    }

    public function edit(ConferenceTest $conferenceTest)
    {
        abort_if(Gate::denies('conference_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.conferenceTests.edit', compact('conferenceTest'));
    }

    public function update(UpdateConferenceTestRequest $request, ConferenceTest $conferenceTest)
    {
        $conferenceTest->update($request->all());

        return redirect()->route('admin.conference-tests.index');

    }

    public function show(ConferenceTest $conferenceTest)
    {
        abort_if(Gate::denies('conference_test_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.conferenceTests.show', compact('conferenceTest'));
    }

    public function destroy(ConferenceTest $conferenceTest)
    {
        abort_if(Gate::denies('conference_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $conferenceTest->delete();

        return back();

    }

    public function massDestroy(MassDestroyConferenceTestRequest $request)
    {
        ConferenceTest::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
