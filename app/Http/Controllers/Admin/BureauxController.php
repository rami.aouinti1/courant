<?php

namespace App\Http\Controllers\Admin;

use App\Model\Department\Bureaux;
use App\Model\Component\Country;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBureauxRequest;
use App\Http\Requests\StoreBureauxRequest;
use App\Http\Requests\UpdateBureauxRequest;
use App\Model\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BureauxController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('bureaux_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bureauxes = Bureaux::all();

        return view('admin.bureauxes.index', compact('bureauxes'));
    }

    public function create()
    {
        abort_if(Gate::denies('bureaux_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $members = User::all()->pluck('name', 'id');

        $countries = Country::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.bureauxes.create', compact('members', 'countries'));
    }

    public function store(StoreBureauxRequest $request)
    {
        $bureaux = Bureaux::create($request->all());
        $bureaux->members()->sync($request->input('members', []));

        return redirect()->route('admin.bureauxes.index');

    }

    public function edit(Bureaux $bureaux)
    {
        abort_if(Gate::denies('bureaux_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $members = User::all()->pluck('name', 'id');

        $countries = Country::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $bureaux->load('members', 'country');

        return view('admin.bureauxes.edit', compact('members', 'countries', 'bureaux'));
    }

    public function update(UpdateBureauxRequest $request, Bureaux $bureaux)
    {
        $bureaux->update($request->all());
        $bureaux->members()->sync($request->input('members', []));

        return redirect()->route('admin.bureauxes.index');

    }

    public function show(Bureaux $bureaux)
    {
        abort_if(Gate::denies('bureaux_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bureaux->load('members', 'country', 'bureauTasks', 'groupRefNews');

        return view('admin.bureauxes.show', compact('bureaux'));
    }

    public function destroy(Bureaux $bureaux)
    {
        abort_if(Gate::denies('bureaux_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $bureaux->delete();

        return back();

    }

    public function massDestroy(MassDestroyBureauxRequest $request)
    {
        Bureaux::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
