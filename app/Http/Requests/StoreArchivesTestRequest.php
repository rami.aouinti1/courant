<?php

namespace App\Http\Requests;

use App\ArchivesTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreArchivesTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('archives_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'archive' => [
                'required'],
        ];

    }
}
