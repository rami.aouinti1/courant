<?php

namespace App\Http\Requests;

use App\ChatTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreChatTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('chat_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'chat' => [
                'required'],
        ];

    }
}
