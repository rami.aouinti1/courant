<?php

namespace App\Http\Requests;

use App\GalleryTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateGalleryTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('gallery_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
        ];

    }
}
