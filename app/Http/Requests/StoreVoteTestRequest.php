<?php

namespace App\Http\Requests;

use App\VoteTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreVoteTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('vote_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'vote' => [
                'required'],
        ];

    }
}
