<?php

namespace App\Http\Requests;

use App\RapportfinancierTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRapportfinancierTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('rapportfinancier_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:rapportfinancier_tests,id',
        ];

    }
}
