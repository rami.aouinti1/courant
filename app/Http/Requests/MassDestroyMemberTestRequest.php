<?php

namespace App\Http\Requests;

use App\MemberTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyMemberTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('member_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:member_tests,id',
        ];

    }
}
