<?php

namespace App\Http\Requests;

use App\DossiersTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyDossiersTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('dossiers_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:dossiers_tests,id',
        ];

    }
}
