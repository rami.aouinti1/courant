<?php

namespace App\Http\Requests;

use App\QuizTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateQuizTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('quiz_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'quiz' => [
                'required'],
        ];

    }
}
