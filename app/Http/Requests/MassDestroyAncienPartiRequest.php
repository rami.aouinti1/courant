<?php

namespace App\Http\Requests;

use App\AncienParti;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyAncienPartiRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ancien_parti_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:ancien_partis,id',
        ];

    }
}
