<?php

namespace App\Http\Requests;

use App\ConferenceTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyConferenceTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('conference_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:conference_tests,id',
        ];

    }
}
