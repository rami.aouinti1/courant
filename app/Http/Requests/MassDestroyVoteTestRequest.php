<?php

namespace App\Http\Requests;

use App\VoteTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyVoteTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('vote_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:vote_tests,id',
        ];

    }
}
