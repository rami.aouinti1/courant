<?php

namespace App\Http\Requests;

use App\ChatTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyChatTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('chat_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:chat_tests,id',
        ];

    }
}
