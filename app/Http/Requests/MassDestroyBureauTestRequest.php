<?php

namespace App\Http\Requests;

use App\BureauTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyBureauTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('bureau_test_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:bureau_tests,id',
        ];

    }
}
