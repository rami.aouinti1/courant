<?php

namespace App\Http\Requests;

use App\Diplom;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreDiplomRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('diplom_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'specialite'  => [
                'required'],
            'degree'      => [
                'required'],
            'ecole'       => [
                'required'],
            'date_beginn' => [
                'required',
                'date_format:' . config('panel.date_format')],
            'date_end'    => [
                'date_format:' . config('panel.date_format'),
                'nullable'],
            'user_id'     => [
                'required',
                'integer'],
        ];

    }
}
