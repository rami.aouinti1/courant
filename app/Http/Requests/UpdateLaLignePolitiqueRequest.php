<?php

namespace App\Http\Requests;

use App\LaLignePolitique;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateLaLignePolitiqueRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('la_ligne_politique_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'title'         => [
                'required'],
            'content'       => [
                'required'],
            'date_creation' => [
                'required',
                'date_format:' . config('panel.date_format')],
        ];

    }
}
