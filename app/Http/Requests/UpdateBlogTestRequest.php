<?php

namespace App\Http\Requests;

use App\BlogTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateBlogTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('blog_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'blog' => [
                'required'],
        ];

    }
}
