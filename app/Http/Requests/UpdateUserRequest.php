<?php

namespace App\Http\Requests;

use App\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'civilite'      => [
                'required'],
            'firstanme'     => [
                'required'],
            'name'          => [
                'required'],
            'date_naiss'    => [
                'date_format:' . config('panel.date_format'),
                'nullable'],
            'email'         => [
                'required',
                'unique:users,email,' . request()->route('user')->id],
            'roles.*'       => [
                'integer'],
            'roles'         => [
                'required',
                'array'],
            'country_id'    => [
                'required',
                'integer'],
            'agree'         => [
                'required'],
            'date_adhesion' => [
                'required',
                'date_format:' . config('panel.date_format')],
            'date_demande'  => [
                'required',
                'date_format:' . config('panel.date_format')],
        ];

    }
}
