<?php

namespace App\Http\Requests;

use App\DocumentationTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateDocumentationTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('documentation_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'docuementation' => [
                'required'],
        ];

    }
}
