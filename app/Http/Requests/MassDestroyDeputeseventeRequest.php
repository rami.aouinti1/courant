<?php

namespace App\Http\Requests;

use App\Deputesevente;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyDeputeseventeRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('deputesevente_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:deputeseventes,id',
        ];

    }
}
