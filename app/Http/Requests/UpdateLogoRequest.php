<?php

namespace App\Http\Requests;

use App\Logo;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateLogoRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('logo_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'title'         => [
                'required'],
            'date_creation' => [
                'required'],
        ];

    }
}
