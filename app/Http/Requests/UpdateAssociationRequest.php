<?php

namespace App\Http\Requests;

use App\Association;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateAssociationRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('association_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'name'        => [
                'required'],
            'date_beginn' => [
                'required',
                'date_format:' . config('panel.date_format')],
            'date_end'    => [
                'date_format:' . config('panel.date_format'),
                'nullable'],
            'user_id'     => [
                'required',
                'integer'],
        ];

    }
}
