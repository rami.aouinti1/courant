<?php

namespace App\Http\Requests;

use App\ConferenceTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreConferenceTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('conference_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'conference' => [
                'required'],
        ];

    }
}
