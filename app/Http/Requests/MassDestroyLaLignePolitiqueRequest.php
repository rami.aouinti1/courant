<?php

namespace App\Http\Requests;

use App\LaLignePolitique;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyLaLignePolitiqueRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('la_ligne_politique_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:la_ligne_politiques,id',
        ];

    }
}
