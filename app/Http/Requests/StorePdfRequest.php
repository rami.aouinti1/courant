<?php

namespace App\Http\Requests;

use App\Pdf;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StorePdfRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('pdf_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'title'  => [
                'required'],
            'file.*' => [
                'required'],
        ];

    }
}
