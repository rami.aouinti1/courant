<?php

namespace App\Http\Requests;

use App\AncienParti;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateAncienPartiRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('ancien_parti_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'parti'       => [
                'required'],
            'date_beginn' => [
                'required',
                'date_format:' . config('panel.date_format')],
            'date_end'    => [
                'required',
                'date_format:' . config('panel.date_format')],
            'user_id'     => [
                'required',
                'integer'],
        ];

    }
}
