<?php

namespace App\Http\Requests;

use App\StatisticesTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreStatisticesTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('statistices_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'statistic' => [
                'required'],
        ];

    }
}
