<?php

namespace App\Http\Requests;

use App\News;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreNewsRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('news_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'author_id'    => [
                'required',
                'integer'],
            'group_ref_id' => [
                'required',
                'integer'],
            'content'      => [
                'required'],
        ];

    }
}
