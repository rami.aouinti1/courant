<?php

namespace App\Http\Requests;

use App\RapportTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreRapportTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('rapport_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'rapport' => [
                'required'],
        ];

    }
}
