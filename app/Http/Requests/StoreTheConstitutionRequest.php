<?php

namespace App\Http\Requests;

use App\TheConstitution;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreTheConstitutionRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('the_constitution_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'constitution' => [
                'required'],
            'content'      => [
                'required'],
            'date'         => [
                'required',
                'date_format:' . config('panel.date_format')],
        ];

    }
}
