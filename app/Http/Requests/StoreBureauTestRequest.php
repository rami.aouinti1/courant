<?php

namespace App\Http\Requests;

use App\BureauTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreBureauTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('bureau_test_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'bureau' => [
                'required'],
        ];

    }
}
