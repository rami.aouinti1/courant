<?php

namespace App\Http\Requests;

use App\TheConstitution;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyTheConstitutionRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('the_constitution_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:the_constitutions,id',
        ];

    }
}
