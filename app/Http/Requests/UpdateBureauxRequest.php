<?php

namespace App\Http\Requests;

use App\Bureaux;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateBureauxRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('bureaux_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'bureau'      => [
                'required'],
            'state'       => [
                'required'],
            'date_beginn' => [
                'required',
                'date_format:' . config('panel.date_format')],
            'date_end'    => [
                'date_format:' . config('panel.date_format'),
                'nullable'],
            'members.*'   => [
                'integer'],
            'members'     => [
                'required',
                'array'],
            'country_id'  => [
                'required',
                'integer'],
        ];

    }
}
