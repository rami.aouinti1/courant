<?php

namespace App\Http\Requests;

use App\RapportfinancierTest;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateRapportfinancierTestRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('rapportfinancier_test_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'rapport_financier' => [
                'required'],
        ];

    }
}
