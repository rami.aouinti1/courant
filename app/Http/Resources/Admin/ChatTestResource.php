<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatTestResource extends JsonResource
{
    public function toArray($request)
    {
        return parent::toArray($request);

    }
}
