<?php

namespace App\Model\Component;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class News extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'news';

    public static $searchable = [
        'content',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'content',
        'author_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'group_ref_id',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);

    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');

    }

    public function group_ref()
    {
        return $this->belongsTo(Bureaux::class, 'group_ref_id');

    }
}
