<?php

namespace App\Model\Component;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
    use SoftDeletes;

    public $table = 'languages';

    public static $searchable = [
        'degree',
        'language',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const DEGREE_SELECT = [
        'Very Good' => 'Very Good',
        'Good'      => 'Good',
        'Basic'     => 'Basic',
    ];

    protected $fillable = [
        'degree',
        'user_id',
        'language',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const LANGUAGE_SELECT = [
        'français' => 'français',
        'Englisch' => 'Englisch',
        'Deutsch'  => 'Deutsch',
        'Other'    => 'Other',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');

    }
}
