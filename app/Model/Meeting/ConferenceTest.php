<?php

namespace App\Model\Meeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConferenceTest extends Model
{
    use SoftDeletes;

    public $table = 'conference_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'conference',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
