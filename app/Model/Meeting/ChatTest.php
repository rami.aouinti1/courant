<?php

namespace App\Model\Meeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChatTest extends Model
{
    use SoftDeletes;

    public $table = 'chat_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'chat',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
