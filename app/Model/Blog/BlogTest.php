<?php

namespace App\Model\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogTest extends Model
{
    use SoftDeletes;

    public $table = 'blog_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'blog',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
