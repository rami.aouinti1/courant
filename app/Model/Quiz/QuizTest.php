<?php

namespace App\Model\Quiz;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuizTest extends Model
{
    use SoftDeletes;

    public $table = 'quiz_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'quiz',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
