<?php

namespace App\Model\Department;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BureauTest extends Model
{
    use SoftDeletes;

    public $table = 'bureau_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'bureau',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
