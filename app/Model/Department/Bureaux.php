<?php

namespace App\Model\Department;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bureaux extends Model
{
    use SoftDeletes;

    public $table = 'bureauxes';

    protected $dates = [
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    public static $searchable = [
        'city',
        'state',
        'bureau',
        'active',
        'date_end',
        'date_beginn',
    ];

    protected $fillable = [
        'city',
        'state',
        'bureau',
        'active',
        'date_end',
        'country_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    const BUREAU_SELECT = [
        'الامين العام المساعد' => 'الامين العام المساعد',
        'الامين المال'         => 'الامين المال',
        'المكتب السياسي'       => 'المكتب السياسي',
        'المكتب التنفيذي'      => 'المكتب التنفيذي',
        'المجلس الوطني'        => 'المجلس الوطني',
        'مكتب الطلبة'          => 'مكتب الطلبة',
        'المكتب الجهوي'        => 'المكتب الجهوي',
        'المكتب المحلي'        => 'المكتب المحلي',
        'لجنة الاتصال'         => 'لجنة الاتصال',
        'النواب'               => 'النواب',
        'المستشارين'           => 'المستشارين',
        'الامين العام'         => 'الامين العام',
    ];

    public function bureauTasks()
    {
        return $this->hasMany(Task::class, 'bureau_id', 'id');

    }

    public function groupRefNews()
    {
        return $this->hasMany(News::class, 'group_ref_id', 'id');

    }

    public function getDateBeginnAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateBeginnAttribute($value)
    {
        $this->attributes['date_beginn'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

    public function getDateEndAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateEndAttribute($value)
    {
        $this->attributes['date_end'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

    public function members()
    {
        return $this->belongsToMany(User::class);

    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');

    }
}
