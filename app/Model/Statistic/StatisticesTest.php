<?php

namespace App\Model\Statistic;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatisticesTest extends Model
{
    use SoftDeletes;

    public $table = 'statistices_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'statistic',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
