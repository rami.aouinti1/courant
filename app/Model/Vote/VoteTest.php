<?php

namespace App\Model\Vote;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VoteTest extends Model
{
    use SoftDeletes;

    public $table = 'vote_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'vote',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
