<?php

namespace App\Model;

use App\Model\Alert\UserAlert;
use App\Model\Component\Country;
use App\Model\Component\Language;
use App\Model\Component\News;
use App\Model\Department\Bureaux;
use App\Model\Event\Task;
use App\Model\Profile\AncienParti;
use App\Model\Profile\Association;
use App\Model\Profile\Certificate;
use App\Model\Profile\Diplom;
use App\Notifications\VerifyUserNotification;
use Carbon\Carbon;
use phpseclib\Crypt\Hash;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;

use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class User extends Authenticatable implements HasMedia
{
    use SoftDeletes, Notifiable, HasApiTokens, HasMediaTrait;

    public $table = 'users';

    protected $appends = [
        'photo',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    const CIVILITE_SELECT = [
        'Femme' => 'Femme',
        'Homme' => 'Homme',
    ];

    public static $searchable = [
        'cin',
        'tel',
        'name',
        'email',
        'ville',
        'region',
        'adresse',
        'civilite',
        'fonction',
        'firstanme',
    ];

    protected $dates = [
        'date_naiss',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_demande',
        'date_adhesion',
        'email_verified_at',
    ];

    const FONCTION_SELECT = [
        'Employee'   => 'Employee',
        'Unemployed' => 'Unemployed',
        'Trainee'    => 'Trainee',
        'Student'    => 'Student',
    ];

    protected $fillable = [
        'tel',
        'cin',
        'name',
        'agree',
        'email',
        'ville',
        'region',
        'adresse',
        'facebook',
        'fonction',
        'civilite',
        'password',
        'approved',
        'firstanme',
        'country_id',
        'date_naiss',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_demande',
        'date_adhesion',
        'remember_token',
        'email_verified_at',
    ];

    public function getIsAdminAttribute()
    {
        return $this->roles()->where('id', 1)->exists();

    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::created(function (User $user) {
            $registrationRole = config('panel.registration_default_role');

            if (!$user->roles()->get()->contains($registrationRole)) {
                $user->roles()->attach($registrationRole);
            }

        });

    }

    public static function boot()
    {
        parent::boot();

        User::observe(new \App\Observers\UserActionObserver);

    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);

    }

    public function userDiploms()
    {
        return $this->hasMany(Diplom::class, 'user_id', 'id');

    }

    public function userCertificates()
    {
        return $this->hasMany(Certificate::class, 'user_id', 'id');

    }

    public function userAssociations()
    {
        return $this->hasMany(Association::class, 'user_id', 'id');

    }

    public function userAncienPartis()
    {
        return $this->hasMany(AncienParti::class, 'user_id', 'id');

    }

    public function userLanguages()
    {
        return $this->hasMany(Language::class, 'user_id', 'id');

    }

    public function authorNews()
    {
        return $this->hasMany(News::class, 'author_id', 'id');

    }

    public function userUserAlerts()
    {
        return $this->belongsToMany(UserAlert::class);

    }

    public function membersBureauxes()
    {
        return $this->belongsToMany(Bureaux::class);

    }

    public function membersTasks()
    {
        return $this->belongsToMany(Task::class);

    }

    public function getDateNaissAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateNaissAttribute($value)
    {
        $this->attributes['date_naiss'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

    public function getEmailVerifiedAtAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;

    }

    public function setEmailVerifiedAtAttribute($value)
    {
        $this->attributes['email_verified_at'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;

    }

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }

    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));

    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);

    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');

    }

    public function getDateAdhesionAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateAdhesionAttribute($value)
    {
        $this->attributes['date_adhesion'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

    public function getPhotoAttribute()
    {
        $file = $this->getMedia('photo')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;

    }

    public function getDateDemandeAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateDemandeAttribute($value)
    {
        $this->attributes['date_demande'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

}
