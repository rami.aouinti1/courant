<?php

namespace App\Model\Library;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class LaLignePolitique extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'la_ligne_politiques';

    public static $searchable = [
        'title',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
        'date_creation',
    ];

    protected $fillable = [
        'title',
        'content',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_creation',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);

    }

    public function getDateCreationAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateCreationAttribute($value)
    {
        $this->attributes['date_creation'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }
}
