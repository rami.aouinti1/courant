<?php

namespace App\Model\File;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RapportTest extends Model
{
    use SoftDeletes;

    public $table = 'rapport_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'rapport',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
