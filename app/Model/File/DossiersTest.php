<?php

namespace App\Model\File;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DossiersTest extends Model
{
    use SoftDeletes;

    public $table = 'dossiers_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'dossier',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
