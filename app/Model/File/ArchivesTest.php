<?php

namespace App\Model\File;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArchivesTest extends Model
{
    use SoftDeletes;

    public $table = 'archives_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'archive',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
