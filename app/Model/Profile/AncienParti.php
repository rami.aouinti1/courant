<?php

namespace App\Model\Profile;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AncienParti extends Model
{
    use SoftDeletes;

    public $table = 'ancien_partis';

    public static $searchable = [
        'parti',
    ];

    protected $dates = [
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    protected $fillable = [
        'parti',
        'user_id',
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    public function getDateBeginnAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateBeginnAttribute($value)
    {
        $this->attributes['date_beginn'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

    public function getDateEndAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateEndAttribute($value)
    {
        $this->attributes['date_end'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');

    }
}
