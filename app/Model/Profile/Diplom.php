<?php

namespace App\Model\Profile;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Diplom extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'diploms';

    protected $appends = [
        'files',
    ];

    public static $searchable = [
        'ecole',
        'degree',
        'specialite',
    ];

    protected $dates = [
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    protected $fillable = [
        'ecole',
        'degree',
        'user_id',
        'date_end',
        'specialite',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    const DEGREE_SELECT = [
        'Licence (Bac+3)'  => 'Licence (Bac+3)',
        'Maitrise (Bac+4)' => 'Maitrise (Bac+4)',
        'Master (Bac +5)'  => 'Master (Bac +5)',
        'Ingenieur'        => 'Ingenieur',
        'Doctor'           => 'Doctor',
        'Formation'        => 'Formation',
        'Stage'            => 'Stage',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);

    }

    public function getDateBeginnAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateBeginnAttribute($value)
    {
        $this->attributes['date_beginn'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

    public function getDateEndAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateEndAttribute($value)
    {
        $this->attributes['date_end'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

    public function getFilesAttribute()
    {
        return $this->getMedia('files');

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');

    }
}
