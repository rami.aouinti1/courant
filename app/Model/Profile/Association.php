<?php

namespace App\Model\Profile;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Association extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'associations';

    public static $searchable = [
        'name',
    ];

    protected $appends = [
        'certification',
    ];

    protected $dates = [
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    protected $fillable = [
        'name',
        'user_id',
        'function',
        'date_end',
        'created_at',
        'updated_at',
        'deleted_at',
        'date_beginn',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);

    }

    public function getDateBeginnAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateBeginnAttribute($value)
    {
        $this->attributes['date_beginn'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

    public function getDateEndAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;

    }

    public function setDateEndAttribute($value)
    {
        $this->attributes['date_end'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;

    }

    public function getCertificationAttribute()
    {
        return $this->getMedia('certification');

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');

    }
}
