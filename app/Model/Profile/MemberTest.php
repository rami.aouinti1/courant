<?php

namespace App\Model\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberTest extends Model
{
    use SoftDeletes;

    public $table = 'member_tests';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'member',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
