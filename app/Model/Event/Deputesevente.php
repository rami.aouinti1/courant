<?php

namespace App\Model\Event;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deputesevente extends Model
{
    use SoftDeletes;

    public $table = 'deputeseventes';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'events',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
