<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAncienPartisTable extends Migration
{
    public function up()
    {
        Schema::create('ancien_partis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parti');
            $table->date('date_beginn');
            $table->date('date_end');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
