<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConferenceTestsTable extends Migration
{
    public function up()
    {
        Schema::create('conference_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('conference');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
