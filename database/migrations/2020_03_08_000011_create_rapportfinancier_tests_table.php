<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRapportfinancierTestsTable extends Migration
{
    public function up()
    {
        Schema::create('rapportfinancier_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rapport_financier');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
