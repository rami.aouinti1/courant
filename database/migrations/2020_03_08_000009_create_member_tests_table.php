<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberTestsTable extends Migration
{
    public function up()
    {
        Schema::create('member_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
