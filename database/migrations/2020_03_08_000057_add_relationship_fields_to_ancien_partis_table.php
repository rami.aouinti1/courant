<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToAncienPartisTable extends Migration
{
    public function up()
    {
        Schema::table('ancien_partis', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'user_fk_1104633')->references('id')->on('users');
        });

    }
}
