<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBureauTestsTable extends Migration
{
    public function up()
    {
        Schema::create('bureau_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bureau');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
