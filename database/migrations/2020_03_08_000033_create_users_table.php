<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable()->unique();
            $table->datetime('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('remember_token')->nullable();
            $table->string('civilite')->nullable();
            $table->string('firstanme')->nullable();
            $table->date('date_naiss')->nullable();
            $table->string('cin')->nullable();
            $table->string('tel')->nullable();
            $table->string('adresse')->nullable();
            $table->string('ville')->nullable();
            $table->string('region')->nullable();
            $table->string('fonction')->nullable();
            $table->string('facebook')->nullable();
            $table->boolean('agree')->default(0)->nullable();
            $table->date('date_adhesion')->nullable();
            $table->date('date_demande')->nullable();
            $table->boolean('approved')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
