<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDossiersTestsTable extends Migration
{
    public function up()
    {
        Schema::create('dossiers_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dossier');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
