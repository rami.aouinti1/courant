<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeputeseventesTable extends Migration
{
    public function up()
    {
        Schema::create('deputeseventes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('events');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
