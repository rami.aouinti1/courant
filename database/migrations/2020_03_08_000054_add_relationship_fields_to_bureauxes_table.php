<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToBureauxesTable extends Migration
{
    public function up()
    {
        Schema::table('bureauxes', function (Blueprint $table) {
            $table->unsignedInteger('country_id');
            $table->foreign('country_id', 'country_fk_1109923')->references('id')->on('countries');
        });

    }
}
