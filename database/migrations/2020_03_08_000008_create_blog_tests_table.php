<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTestsTable extends Migration
{
    public function up()
    {
        Schema::create('blog_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('blog');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
