<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatTestsTable extends Migration
{
    public function up()
    {
        Schema::create('chat_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chat');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
