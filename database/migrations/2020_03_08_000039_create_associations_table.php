<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssociationsTable extends Migration
{
    public function up()
    {
        Schema::create('associations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('function')->nullable();
            $table->date('date_beginn');
            $table->date('date_end')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
