<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiplomsTable extends Migration
{
    public function up()
    {
        Schema::create('diploms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('degree');
            $table->string('ecole');
            $table->date('date_beginn');
            $table->date('date_end')->nullable();
            $table->string('specialite');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
