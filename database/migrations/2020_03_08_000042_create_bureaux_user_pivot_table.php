<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBureauxUserPivotTable extends Migration
{
    public function up()
    {
        Schema::create('bureaux_user', function (Blueprint $table) {
            $table->unsignedInteger('bureaux_id');
            $table->foreign('bureaux_id', 'bureaux_id_fk_1104125')->references('id')->on('bureauxes')->onDelete('cascade');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'user_id_fk_1104125')->references('id')->on('users')->onDelete('cascade');
        });

    }
}
