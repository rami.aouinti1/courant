<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRapportTestsTable extends Migration
{
    public function up()
    {
        Schema::create('rapport_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rapport');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
