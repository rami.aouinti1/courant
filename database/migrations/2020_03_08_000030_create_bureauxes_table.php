<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBureauxesTable extends Migration
{
    public function up()
    {
        Schema::create('bureauxes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bureau');
            $table->string('state');
            $table->string('city')->nullable();
            $table->date('date_beginn');
            $table->date('date_end')->nullable();
            $table->boolean('active')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
