<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizTestsTable extends Migration
{
    public function up()
    {
        Schema::create('quiz_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quiz');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
