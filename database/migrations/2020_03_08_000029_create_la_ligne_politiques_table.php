<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaLignePolitiquesTable extends Migration
{
    public function up()
    {
        Schema::create('la_ligne_politiques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('content');
            $table->date('date_creation');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
