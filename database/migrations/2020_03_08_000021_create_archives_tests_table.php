<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArchivesTestsTable extends Migration
{
    public function up()
    {
        Schema::create('archives_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('archive');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
