<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToNewsTable extends Migration
{
    public function up()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->unsignedInteger('author_id');
            $table->foreign('author_id', 'author_fk_1110522')->references('id')->on('users');
            $table->unsignedInteger('group_ref_id');
            $table->foreign('group_ref_id', 'group_ref_fk_1110523')->references('id')->on('bureauxes');
        });

    }
}
