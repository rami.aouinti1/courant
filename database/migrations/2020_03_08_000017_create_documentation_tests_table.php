<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentationTestsTable extends Migration
{
    public function up()
    {
        Schema::create('documentation_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('docuementation');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
