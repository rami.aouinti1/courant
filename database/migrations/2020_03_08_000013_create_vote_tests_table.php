<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoteTestsTable extends Migration
{
    public function up()
    {
        Schema::create('vote_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vote');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
