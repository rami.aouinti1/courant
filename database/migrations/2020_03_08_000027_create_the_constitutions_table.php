<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTheConstitutionsTable extends Migration
{
    public function up()
    {
        Schema::create('the_constitutions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('constitution');
            $table->longText('content');
            $table->date('date');
            $table->timestamps();
            $table->softDeletes();
        });

    }
}
