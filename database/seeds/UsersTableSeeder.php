<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$Ik3f7c9CcPx3nP1Juer2zudtGJ.lOd.wQunuABHBH6lvDiYqGzj7u',
                'remember_token' => null,
                'approved'       => 1,
                'firstanme'      => '',
                'cin'            => '',
                'tel'            => '',
                'adresse'        => '',
                'ville'          => '',
                'region'         => '',
                'facebook'       => '',
            ],
        ];

        User::insert($users);

    }
}
