<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => '1',
                'title' => 'user_management_access',
            ],
            [
                'id'    => '2',
                'title' => 'permission_create',
            ],
            [
                'id'    => '3',
                'title' => 'permission_edit',
            ],
            [
                'id'    => '4',
                'title' => 'permission_show',
            ],
            [
                'id'    => '5',
                'title' => 'permission_delete',
            ],
            [
                'id'    => '6',
                'title' => 'permission_access',
            ],
            [
                'id'    => '7',
                'title' => 'role_create',
            ],
            [
                'id'    => '8',
                'title' => 'role_edit',
            ],
            [
                'id'    => '9',
                'title' => 'role_show',
            ],
            [
                'id'    => '10',
                'title' => 'role_delete',
            ],
            [
                'id'    => '11',
                'title' => 'role_access',
            ],
            [
                'id'    => '12',
                'title' => 'user_create',
            ],
            [
                'id'    => '13',
                'title' => 'user_edit',
            ],
            [
                'id'    => '14',
                'title' => 'user_show',
            ],
            [
                'id'    => '15',
                'title' => 'user_delete',
            ],
            [
                'id'    => '16',
                'title' => 'user_access',
            ],
            [
                'id'    => '17',
                'title' => 'audit_log_show',
            ],
            [
                'id'    => '18',
                'title' => 'audit_log_access',
            ],
            [
                'id'    => '19',
                'title' => 'user_alert_create',
            ],
            [
                'id'    => '20',
                'title' => 'user_alert_show',
            ],
            [
                'id'    => '21',
                'title' => 'user_alert_delete',
            ],
            [
                'id'    => '22',
                'title' => 'user_alert_access',
            ],
            [
                'id'    => '23',
                'title' => 'task_management_access',
            ],
            [
                'id'    => '24',
                'title' => 'task_status_create',
            ],
            [
                'id'    => '25',
                'title' => 'task_status_edit',
            ],
            [
                'id'    => '26',
                'title' => 'task_status_show',
            ],
            [
                'id'    => '27',
                'title' => 'task_status_delete',
            ],
            [
                'id'    => '28',
                'title' => 'task_status_access',
            ],
            [
                'id'    => '29',
                'title' => 'task_tag_create',
            ],
            [
                'id'    => '30',
                'title' => 'task_tag_edit',
            ],
            [
                'id'    => '31',
                'title' => 'task_tag_show',
            ],
            [
                'id'    => '32',
                'title' => 'task_tag_delete',
            ],
            [
                'id'    => '33',
                'title' => 'task_tag_access',
            ],
            [
                'id'    => '34',
                'title' => 'task_create',
            ],
            [
                'id'    => '35',
                'title' => 'task_edit',
            ],
            [
                'id'    => '36',
                'title' => 'task_show',
            ],
            [
                'id'    => '37',
                'title' => 'task_delete',
            ],
            [
                'id'    => '38',
                'title' => 'task_access',
            ],
            [
                'id'    => '39',
                'title' => 'tasks_calendar_access',
            ],
            [
                'id'    => '40',
                'title' => 'depatement_access',
            ],
            [
                'id'    => '41',
                'title' => 'bureaux_create',
            ],
            [
                'id'    => '42',
                'title' => 'bureaux_edit',
            ],
            [
                'id'    => '43',
                'title' => 'bureaux_show',
            ],
            [
                'id'    => '44',
                'title' => 'bureaux_delete',
            ],
            [
                'id'    => '45',
                'title' => 'bureaux_access',
            ],
            [
                'id'    => '46',
                'title' => 'library_access',
            ],
            [
                'id'    => '47',
                'title' => 'courant_democrate_access',
            ],
            [
                'id'    => '48',
                'title' => 'tunisium_access',
            ],
            [
                'id'    => '49',
                'title' => 'statistic_access',
            ],
            [
                'id'    => '50',
                'title' => 'diplom_create',
            ],
            [
                'id'    => '51',
                'title' => 'diplom_edit',
            ],
            [
                'id'    => '52',
                'title' => 'diplom_show',
            ],
            [
                'id'    => '53',
                'title' => 'diplom_delete',
            ],
            [
                'id'    => '54',
                'title' => 'diplom_access',
            ],
            [
                'id'    => '55',
                'title' => 'certificate_create',
            ],
            [
                'id'    => '56',
                'title' => 'certificate_edit',
            ],
            [
                'id'    => '57',
                'title' => 'certificate_show',
            ],
            [
                'id'    => '58',
                'title' => 'certificate_delete',
            ],
            [
                'id'    => '59',
                'title' => 'certificate_access',
            ],
            [
                'id'    => '60',
                'title' => 'association_create',
            ],
            [
                'id'    => '61',
                'title' => 'association_edit',
            ],
            [
                'id'    => '62',
                'title' => 'association_show',
            ],
            [
                'id'    => '63',
                'title' => 'association_delete',
            ],
            [
                'id'    => '64',
                'title' => 'association_access',
            ],
            [
                'id'    => '65',
                'title' => 'ancien_parti_create',
            ],
            [
                'id'    => '66',
                'title' => 'ancien_parti_edit',
            ],
            [
                'id'    => '67',
                'title' => 'ancien_parti_show',
            ],
            [
                'id'    => '68',
                'title' => 'ancien_parti_delete',
            ],
            [
                'id'    => '69',
                'title' => 'ancien_parti_access',
            ],
            [
                'id'    => '70',
                'title' => 'la_ligne_politique_create',
            ],
            [
                'id'    => '71',
                'title' => 'la_ligne_politique_edit',
            ],
            [
                'id'    => '72',
                'title' => 'la_ligne_politique_show',
            ],
            [
                'id'    => '73',
                'title' => 'la_ligne_politique_delete',
            ],
            [
                'id'    => '74',
                'title' => 'la_ligne_politique_access',
            ],
            [
                'id'    => '75',
                'title' => 'logo_create',
            ],
            [
                'id'    => '76',
                'title' => 'logo_edit',
            ],
            [
                'id'    => '77',
                'title' => 'logo_show',
            ],
            [
                'id'    => '78',
                'title' => 'logo_delete',
            ],
            [
                'id'    => '79',
                'title' => 'logo_access',
            ],
            [
                'id'    => '80',
                'title' => 'the_constitution_create',
            ],
            [
                'id'    => '81',
                'title' => 'the_constitution_edit',
            ],
            [
                'id'    => '82',
                'title' => 'the_constitution_show',
            ],
            [
                'id'    => '83',
                'title' => 'the_constitution_delete',
            ],
            [
                'id'    => '84',
                'title' => 'the_constitution_access',
            ],
            [
                'id'    => '85',
                'title' => 'quiz_access',
            ],
            [
                'id'    => '86',
                'title' => 'other_access',
            ],
            [
                'id'    => '87',
                'title' => 'video_create',
            ],
            [
                'id'    => '88',
                'title' => 'video_edit',
            ],
            [
                'id'    => '89',
                'title' => 'video_show',
            ],
            [
                'id'    => '90',
                'title' => 'video_delete',
            ],
            [
                'id'    => '91',
                'title' => 'video_access',
            ],
            [
                'id'    => '92',
                'title' => 'pdf_create',
            ],
            [
                'id'    => '93',
                'title' => 'pdf_edit',
            ],
            [
                'id'    => '94',
                'title' => 'pdf_show',
            ],
            [
                'id'    => '95',
                'title' => 'pdf_delete',
            ],
            [
                'id'    => '96',
                'title' => 'pdf_access',
            ],
            [
                'id'    => '97',
                'title' => 'bureauregional_access',
            ],
            [
                'id'    => '98',
                'title' => 'chat_access',
            ],
            [
                'id'    => '99',
                'title' => 'blog_access',
            ],
            [
                'id'    => '100',
                'title' => 'member_access',
            ],
            [
                'id'    => '101',
                'title' => 'bureau_access',
            ],
            [
                'id'    => '102',
                'title' => 'rapport_access',
            ],
            [
                'id'    => '103',
                'title' => 'rapport_financier_access',
            ],
            [
                'id'    => '104',
                'title' => 'vote_access',
            ],
            [
                'id'    => '105',
                'title' => 'conference_access',
            ],
            [
                'id'    => '106',
                'title' => 'gallery_access',
            ],
            [
                'id'    => '107',
                'title' => 'archive_access',
            ],
            [
                'id'    => '108',
                'title' => 'depute_access',
            ],
            [
                'id'    => '109',
                'title' => 'event_access',
            ],
            [
                'id'    => '110',
                'title' => 'dossier_access',
            ],
            [
                'id'    => '111',
                'title' => 'language_create',
            ],
            [
                'id'    => '112',
                'title' => 'language_edit',
            ],
            [
                'id'    => '113',
                'title' => 'language_show',
            ],
            [
                'id'    => '114',
                'title' => 'language_delete',
            ],
            [
                'id'    => '115',
                'title' => 'language_access',
            ],
            [
                'id'    => '116',
                'title' => 'documentation_access',
            ],
            [
                'id'    => '117',
                'title' => 'country_create',
            ],
            [
                'id'    => '118',
                'title' => 'country_edit',
            ],
            [
                'id'    => '119',
                'title' => 'country_show',
            ],
            [
                'id'    => '120',
                'title' => 'country_delete',
            ],
            [
                'id'    => '121',
                'title' => 'country_access',
            ],
            [
                'id'    => '122',
                'title' => 'chat_test_create',
            ],
            [
                'id'    => '123',
                'title' => 'chat_test_edit',
            ],
            [
                'id'    => '124',
                'title' => 'chat_test_show',
            ],
            [
                'id'    => '125',
                'title' => 'chat_test_delete',
            ],
            [
                'id'    => '126',
                'title' => 'chat_test_access',
            ],
            [
                'id'    => '127',
                'title' => 'blog_test_create',
            ],
            [
                'id'    => '128',
                'title' => 'blog_test_edit',
            ],
            [
                'id'    => '129',
                'title' => 'blog_test_show',
            ],
            [
                'id'    => '130',
                'title' => 'blog_test_delete',
            ],
            [
                'id'    => '131',
                'title' => 'blog_test_access',
            ],
            [
                'id'    => '132',
                'title' => 'member_test_create',
            ],
            [
                'id'    => '133',
                'title' => 'member_test_edit',
            ],
            [
                'id'    => '134',
                'title' => 'member_test_show',
            ],
            [
                'id'    => '135',
                'title' => 'member_test_delete',
            ],
            [
                'id'    => '136',
                'title' => 'member_test_access',
            ],
            [
                'id'    => '137',
                'title' => 'bureau_test_create',
            ],
            [
                'id'    => '138',
                'title' => 'bureau_test_edit',
            ],
            [
                'id'    => '139',
                'title' => 'bureau_test_show',
            ],
            [
                'id'    => '140',
                'title' => 'bureau_test_delete',
            ],
            [
                'id'    => '141',
                'title' => 'bureau_test_access',
            ],
            [
                'id'    => '142',
                'title' => 'rapportfinancier_test_create',
            ],
            [
                'id'    => '143',
                'title' => 'rapportfinancier_test_edit',
            ],
            [
                'id'    => '144',
                'title' => 'rapportfinancier_test_show',
            ],
            [
                'id'    => '145',
                'title' => 'rapportfinancier_test_delete',
            ],
            [
                'id'    => '146',
                'title' => 'rapportfinancier_test_access',
            ],
            [
                'id'    => '147',
                'title' => 'rapport_test_create',
            ],
            [
                'id'    => '148',
                'title' => 'rapport_test_edit',
            ],
            [
                'id'    => '149',
                'title' => 'rapport_test_show',
            ],
            [
                'id'    => '150',
                'title' => 'rapport_test_delete',
            ],
            [
                'id'    => '151',
                'title' => 'rapport_test_access',
            ],
            [
                'id'    => '152',
                'title' => 'vote_test_create',
            ],
            [
                'id'    => '153',
                'title' => 'vote_test_edit',
            ],
            [
                'id'    => '154',
                'title' => 'vote_test_show',
            ],
            [
                'id'    => '155',
                'title' => 'vote_test_delete',
            ],
            [
                'id'    => '156',
                'title' => 'vote_test_access',
            ],
            [
                'id'    => '157',
                'title' => 'conference_test_create',
            ],
            [
                'id'    => '158',
                'title' => 'conference_test_edit',
            ],
            [
                'id'    => '159',
                'title' => 'conference_test_show',
            ],
            [
                'id'    => '160',
                'title' => 'conference_test_delete',
            ],
            [
                'id'    => '161',
                'title' => 'conference_test_access',
            ],
            [
                'id'    => '162',
                'title' => 'gallery_test_create',
            ],
            [
                'id'    => '163',
                'title' => 'gallery_test_edit',
            ],
            [
                'id'    => '164',
                'title' => 'gallery_test_show',
            ],
            [
                'id'    => '165',
                'title' => 'gallery_test_delete',
            ],
            [
                'id'    => '166',
                'title' => 'gallery_test_access',
            ],
            [
                'id'    => '167',
                'title' => 'deputesevente_create',
            ],
            [
                'id'    => '168',
                'title' => 'deputesevente_edit',
            ],
            [
                'id'    => '169',
                'title' => 'deputesevente_show',
            ],
            [
                'id'    => '170',
                'title' => 'deputesevente_delete',
            ],
            [
                'id'    => '171',
                'title' => 'deputesevente_access',
            ],
            [
                'id'    => '172',
                'title' => 'documentation_test_create',
            ],
            [
                'id'    => '173',
                'title' => 'documentation_test_edit',
            ],
            [
                'id'    => '174',
                'title' => 'documentation_test_show',
            ],
            [
                'id'    => '175',
                'title' => 'documentation_test_delete',
            ],
            [
                'id'    => '176',
                'title' => 'documentation_test_access',
            ],
            [
                'id'    => '177',
                'title' => 'dossiers_test_create',
            ],
            [
                'id'    => '178',
                'title' => 'dossiers_test_edit',
            ],
            [
                'id'    => '179',
                'title' => 'dossiers_test_show',
            ],
            [
                'id'    => '180',
                'title' => 'dossiers_test_delete',
            ],
            [
                'id'    => '181',
                'title' => 'dossiers_test_access',
            ],
            [
                'id'    => '182',
                'title' => 'quiz_test_create',
            ],
            [
                'id'    => '183',
                'title' => 'quiz_test_edit',
            ],
            [
                'id'    => '184',
                'title' => 'quiz_test_show',
            ],
            [
                'id'    => '185',
                'title' => 'quiz_test_delete',
            ],
            [
                'id'    => '186',
                'title' => 'quiz_test_access',
            ],
            [
                'id'    => '187',
                'title' => 'statistices_test_create',
            ],
            [
                'id'    => '188',
                'title' => 'statistices_test_edit',
            ],
            [
                'id'    => '189',
                'title' => 'statistices_test_show',
            ],
            [
                'id'    => '190',
                'title' => 'statistices_test_delete',
            ],
            [
                'id'    => '191',
                'title' => 'statistices_test_access',
            ],
            [
                'id'    => '192',
                'title' => 'archives_test_create',
            ],
            [
                'id'    => '193',
                'title' => 'archives_test_edit',
            ],
            [
                'id'    => '194',
                'title' => 'archives_test_show',
            ],
            [
                'id'    => '195',
                'title' => 'archives_test_delete',
            ],
            [
                'id'    => '196',
                'title' => 'archives_test_access',
            ],
            [
                'id'    => '197',
                'title' => 'news_create',
            ],
            [
                'id'    => '198',
                'title' => 'news_edit',
            ],
            [
                'id'    => '199',
                'title' => 'news_show',
            ],
            [
                'id'    => '200',
                'title' => 'news_delete',
            ],
            [
                'id'    => '201',
                'title' => 'news_access',
            ],
            [
                'id'    => '202',
                'title' => 'basic_c_r_m_access',
            ],
            [
                'id'    => '203',
                'title' => 'crm_status_create',
            ],
            [
                'id'    => '204',
                'title' => 'crm_status_edit',
            ],
            [
                'id'    => '205',
                'title' => 'crm_status_show',
            ],
            [
                'id'    => '206',
                'title' => 'crm_status_delete',
            ],
            [
                'id'    => '207',
                'title' => 'crm_status_access',
            ],
            [
                'id'    => '208',
                'title' => 'crm_customer_create',
            ],
            [
                'id'    => '209',
                'title' => 'crm_customer_edit',
            ],
            [
                'id'    => '210',
                'title' => 'crm_customer_show',
            ],
            [
                'id'    => '211',
                'title' => 'crm_customer_delete',
            ],
            [
                'id'    => '212',
                'title' => 'crm_customer_access',
            ],
            [
                'id'    => '213',
                'title' => 'crm_note_create',
            ],
            [
                'id'    => '214',
                'title' => 'crm_note_edit',
            ],
            [
                'id'    => '215',
                'title' => 'crm_note_show',
            ],
            [
                'id'    => '216',
                'title' => 'crm_note_delete',
            ],
            [
                'id'    => '217',
                'title' => 'crm_note_access',
            ],
            [
                'id'    => '218',
                'title' => 'crm_document_create',
            ],
            [
                'id'    => '219',
                'title' => 'crm_document_edit',
            ],
            [
                'id'    => '220',
                'title' => 'crm_document_show',
            ],
            [
                'id'    => '221',
                'title' => 'crm_document_delete',
            ],
            [
                'id'    => '222',
                'title' => 'crm_document_access',
            ],
        ];

        Permission::insert($permissions);

    }
}
