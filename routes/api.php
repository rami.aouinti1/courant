<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::post('users/media', 'UsersApiController@storeMedia')->name('users.storeMedia');
    Route::apiResource('users', 'UsersApiController');

    // Task Statuses
    Route::apiResource('task-statuses', 'TaskStatusApiController');

    // Task Tags
    Route::apiResource('task-tags', 'TaskTagApiController');

    // Tasks
    Route::post('tasks/media', 'TaskApiController@storeMedia')->name('tasks.storeMedia');
    Route::apiResource('tasks', 'TaskApiController');

    // Bureauxes
    Route::apiResource('bureauxes', 'BureauxApiController');

    // Diploms
    Route::post('diploms/media', 'DiplomApiController@storeMedia')->name('diploms.storeMedia');
    Route::apiResource('diploms', 'DiplomApiController');

    // Certificates
    Route::post('certificates/media', 'CertificateApiController@storeMedia')->name('certificates.storeMedia');
    Route::apiResource('certificates', 'CertificateApiController');

    // Associations
    Route::post('associations/media', 'AssociationApiController@storeMedia')->name('associations.storeMedia');
    Route::apiResource('associations', 'AssociationApiController');

    // Ancien Partis
    Route::apiResource('ancien-partis', 'AncienPartiApiController');

    // La Ligne Politiques
    Route::post('la-ligne-politiques/media', 'LaLignePolitiqueApiController@storeMedia')->name('la-ligne-politiques.storeMedia');
    Route::apiResource('la-ligne-politiques', 'LaLignePolitiqueApiController');

    // Logos
    Route::post('logos/media', 'LogoApiController@storeMedia')->name('logos.storeMedia');
    Route::apiResource('logos', 'LogoApiController');

    // The Constitutions
    Route::post('the-constitutions/media', 'TheConstitutionApiController@storeMedia')->name('the-constitutions.storeMedia');
    Route::apiResource('the-constitutions', 'TheConstitutionApiController');

    // Videos
    Route::apiResource('videos', 'VideoApiController');

    // Pdfs
    Route::post('pdfs/media', 'PdfApiController@storeMedia')->name('pdfs.storeMedia');
    Route::apiResource('pdfs', 'PdfApiController');

    // Languages
    Route::apiResource('languages', 'LanguageApiController');

    // Countries
    Route::apiResource('countries', 'CountriesApiController');

    // Chat Tests
    Route::apiResource('chat-tests', 'ChatTestApiController');

    // Blog Tests
    Route::apiResource('blog-tests', 'BlogTestApiController');

    // Member Tests
    Route::apiResource('member-tests', 'MemberTestApiController');

    // Bureau Tests
    Route::apiResource('bureau-tests', 'BureauTestApiController');

    // Rapportfinancier Tests
    Route::apiResource('rapportfinancier-tests', 'RapportfinancierTestApiController');

    // Rapport Tests
    Route::apiResource('rapport-tests', 'RapportTestApiController');

    // Vote Tests
    Route::apiResource('vote-tests', 'VoteTestApiController');

    // Conference Tests
    Route::apiResource('conference-tests', 'ConferenceTestApiController');

    // Gallery Tests
    Route::post('gallery-tests/media', 'GalleryTestApiController@storeMedia')->name('gallery-tests.storeMedia');
    Route::apiResource('gallery-tests', 'GalleryTestApiController');

    // Deputeseventes
    Route::apiResource('deputeseventes', 'DeputeseventesApiController');

    // Documentation Tests
    Route::apiResource('documentation-tests', 'DocumentationTestApiController');

    // Dossiers Tests
    Route::apiResource('dossiers-tests', 'DossiersTestApiController');

    // Quiz Tests
    Route::apiResource('quiz-tests', 'QuizTestApiController');

    // Statistices Tests
    Route::apiResource('statistices-tests', 'StatisticesTestApiController');

    // Archives Tests
    Route::apiResource('archives-tests', 'ArchivesTestApiController');

    // News
    Route::post('news/media', 'NewsApiController@storeMedia')->name('news.storeMedia');
    Route::apiResource('news', 'NewsApiController');

    // Crm Statuses
    Route::apiResource('crm-statuses', 'CrmStatusApiController');

    // Crm Customers
    Route::apiResource('crm-customers', 'CrmCustomerApiController');

    // Crm Notes
    Route::apiResource('crm-notes', 'CrmNoteApiController');

    // Crm Documents
    Route::post('crm-documents/media', 'CrmDocumentApiController@storeMedia')->name('crm-documents.storeMedia');
    Route::apiResource('crm-documents', 'CrmDocumentApiController');

});
