<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes();
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('user-alerts/read', 'UserAlertsController@read');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::post('users/media', 'UsersController@storeMedia')->name('users.storeMedia');
    Route::post('users/ckmedia', 'UsersController@storeCKEditorImages')->name('users.storeCKEditorImages');
    Route::resource('users', 'UsersController');

    // Audit Logs
    Route::resource('audit-logs', 'AuditLogsController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);

    // User Alerts
    Route::delete('user-alerts/destroy', 'UserAlertsController@massDestroy')->name('user-alerts.massDestroy');
    Route::resource('user-alerts', 'UserAlertsController', ['except' => ['edit', 'update']]);

    // Task Statuses
    Route::delete('task-statuses/destroy', 'TaskStatusController@massDestroy')->name('task-statuses.massDestroy');
    Route::resource('task-statuses', 'TaskStatusController');

    // Task Tags
    Route::delete('task-tags/destroy', 'TaskTagController@massDestroy')->name('task-tags.massDestroy');
    Route::resource('task-tags', 'TaskTagController');

    // Tasks
    Route::delete('tasks/destroy', 'TaskController@massDestroy')->name('tasks.massDestroy');
    Route::post('tasks/media', 'TaskController@storeMedia')->name('tasks.storeMedia');
    Route::post('tasks/ckmedia', 'TaskController@storeCKEditorImages')->name('tasks.storeCKEditorImages');
    Route::resource('tasks', 'TaskController');

    // Tasks Calendars
    Route::resource('tasks-calendars', 'TasksCalendarController', ['except' => ['create', 'store', 'edit', 'update', 'show', 'destroy']]);

    // Bureauxes
    Route::delete('bureauxes/destroy', 'BureauxController@massDestroy')->name('bureauxes.massDestroy');
    Route::resource('bureauxes', 'BureauxController');

    // Diploms
    Route::delete('diploms/destroy', 'DiplomController@massDestroy')->name('diploms.massDestroy');
    Route::post('diploms/media', 'DiplomController@storeMedia')->name('diploms.storeMedia');
    Route::post('diploms/ckmedia', 'DiplomController@storeCKEditorImages')->name('diploms.storeCKEditorImages');
    Route::resource('diploms', 'DiplomController');

    // Certificates
    Route::delete('certificates/destroy', 'CertificateController@massDestroy')->name('certificates.massDestroy');
    Route::post('certificates/media', 'CertificateController@storeMedia')->name('certificates.storeMedia');
    Route::post('certificates/ckmedia', 'CertificateController@storeCKEditorImages')->name('certificates.storeCKEditorImages');
    Route::resource('certificates', 'CertificateController');

    // Associations
    Route::delete('associations/destroy', 'AssociationController@massDestroy')->name('associations.massDestroy');
    Route::post('associations/media', 'AssociationController@storeMedia')->name('associations.storeMedia');
    Route::post('associations/ckmedia', 'AssociationController@storeCKEditorImages')->name('associations.storeCKEditorImages');
    Route::resource('associations', 'AssociationController');

    // Ancien Partis
    Route::delete('ancien-partis/destroy', 'AncienPartiController@massDestroy')->name('ancien-partis.massDestroy');
    Route::resource('ancien-partis', 'AncienPartiController');

    // La Ligne Politiques
    Route::delete('la-ligne-politiques/destroy', 'LaLignePolitiqueController@massDestroy')->name('la-ligne-politiques.massDestroy');
    Route::post('la-ligne-politiques/media', 'LaLignePolitiqueController@storeMedia')->name('la-ligne-politiques.storeMedia');
    Route::post('la-ligne-politiques/ckmedia', 'LaLignePolitiqueController@storeCKEditorImages')->name('la-ligne-politiques.storeCKEditorImages');
    Route::resource('la-ligne-politiques', 'LaLignePolitiqueController');

    // Logos
    Route::delete('logos/destroy', 'LogoController@massDestroy')->name('logos.massDestroy');
    Route::post('logos/media', 'LogoController@storeMedia')->name('logos.storeMedia');
    Route::post('logos/ckmedia', 'LogoController@storeCKEditorImages')->name('logos.storeCKEditorImages');
    Route::resource('logos', 'LogoController');

    // The Constitutions
    Route::delete('the-constitutions/destroy', 'TheConstitutionController@massDestroy')->name('the-constitutions.massDestroy');
    Route::post('the-constitutions/media', 'TheConstitutionController@storeMedia')->name('the-constitutions.storeMedia');
    Route::post('the-constitutions/ckmedia', 'TheConstitutionController@storeCKEditorImages')->name('the-constitutions.storeCKEditorImages');
    Route::resource('the-constitutions', 'TheConstitutionController');

    // Videos
    Route::delete('videos/destroy', 'VideoController@massDestroy')->name('videos.massDestroy');
    Route::resource('videos', 'VideoController');

    // Pdfs
    Route::delete('pdfs/destroy', 'PdfController@massDestroy')->name('pdfs.massDestroy');
    Route::post('pdfs/media', 'PdfController@storeMedia')->name('pdfs.storeMedia');
    Route::post('pdfs/ckmedia', 'PdfController@storeCKEditorImages')->name('pdfs.storeCKEditorImages');
    Route::resource('pdfs', 'PdfController');

    // Languages
    Route::delete('languages/destroy', 'LanguageController@massDestroy')->name('languages.massDestroy');
    Route::resource('languages', 'LanguageController');

    // Countries
    Route::delete('countries/destroy', 'CountriesController@massDestroy')->name('countries.massDestroy');
    Route::resource('countries', 'CountriesController');

    // Chat Tests
    Route::delete('chat-tests/destroy', 'ChatTestController@massDestroy')->name('chat-tests.massDestroy');
    Route::resource('chat-tests', 'ChatTestController');

    // Blog Tests
    Route::delete('blog-tests/destroy', 'BlogTestController@massDestroy')->name('blog-tests.massDestroy');
    Route::resource('blog-tests', 'BlogTestController');

    // Member Tests
    Route::delete('member-tests/destroy', 'MemberTestController@massDestroy')->name('member-tests.massDestroy');
    Route::resource('member-tests', 'MemberTestController');

    // Bureau Tests
    Route::delete('bureau-tests/destroy', 'BureauTestController@massDestroy')->name('bureau-tests.massDestroy');
    Route::resource('bureau-tests', 'BureauTestController');

    // Rapportfinancier Tests
    Route::delete('rapportfinancier-tests/destroy', 'RapportfinancierTestController@massDestroy')->name('rapportfinancier-tests.massDestroy');
    Route::resource('rapportfinancier-tests', 'RapportfinancierTestController');

    // Rapport Tests
    Route::delete('rapport-tests/destroy', 'RapportTestController@massDestroy')->name('rapport-tests.massDestroy');
    Route::resource('rapport-tests', 'RapportTestController');

    // Vote Tests
    Route::delete('vote-tests/destroy', 'VoteTestController@massDestroy')->name('vote-tests.massDestroy');
    Route::resource('vote-tests', 'VoteTestController');

    // Conference Tests
    Route::delete('conference-tests/destroy', 'ConferenceTestController@massDestroy')->name('conference-tests.massDestroy');
    Route::resource('conference-tests', 'ConferenceTestController');

    // Gallery Tests
    Route::delete('gallery-tests/destroy', 'GalleryTestController@massDestroy')->name('gallery-tests.massDestroy');
    Route::post('gallery-tests/media', 'GalleryTestController@storeMedia')->name('gallery-tests.storeMedia');
    Route::post('gallery-tests/ckmedia', 'GalleryTestController@storeCKEditorImages')->name('gallery-tests.storeCKEditorImages');
    Route::resource('gallery-tests', 'GalleryTestController');

    // Deputeseventes
    Route::delete('deputeseventes/destroy', 'DeputeseventesController@massDestroy')->name('deputeseventes.massDestroy');
    Route::resource('deputeseventes', 'DeputeseventesController');

    // Documentation Tests
    Route::delete('documentation-tests/destroy', 'DocumentationTestController@massDestroy')->name('documentation-tests.massDestroy');
    Route::resource('documentation-tests', 'DocumentationTestController');

    // Dossiers Tests
    Route::delete('dossiers-tests/destroy', 'DossiersTestController@massDestroy')->name('dossiers-tests.massDestroy');
    Route::resource('dossiers-tests', 'DossiersTestController');

    // Quiz Tests
    Route::delete('quiz-tests/destroy', 'QuizTestController@massDestroy')->name('quiz-tests.massDestroy');
    Route::resource('quiz-tests', 'QuizTestController');

    // Statistices Tests
    Route::delete('statistices-tests/destroy', 'StatisticesTestController@massDestroy')->name('statistices-tests.massDestroy');
    Route::resource('statistices-tests', 'StatisticesTestController');

    // Archives Tests
    Route::delete('archives-tests/destroy', 'ArchivesTestController@massDestroy')->name('archives-tests.massDestroy');
    Route::resource('archives-tests', 'ArchivesTestController');

    // News
    Route::delete('news/destroy', 'NewsController@massDestroy')->name('news.massDestroy');
    Route::post('news/media', 'NewsController@storeMedia')->name('news.storeMedia');
    Route::post('news/ckmedia', 'NewsController@storeCKEditorImages')->name('news.storeCKEditorImages');
    Route::resource('news', 'NewsController');

    // Crm Statuses
    Route::delete('crm-statuses/destroy', 'CrmStatusController@massDestroy')->name('crm-statuses.massDestroy');
    Route::resource('crm-statuses', 'CrmStatusController');

    // Crm Customers
    Route::delete('crm-customers/destroy', 'CrmCustomerController@massDestroy')->name('crm-customers.massDestroy');
    Route::resource('crm-customers', 'CrmCustomerController');

    // Crm Notes
    Route::delete('crm-notes/destroy', 'CrmNoteController@massDestroy')->name('crm-notes.massDestroy');
    Route::resource('crm-notes', 'CrmNoteController');

    // Crm Documents
    Route::delete('crm-documents/destroy', 'CrmDocumentController@massDestroy')->name('crm-documents.massDestroy');
    Route::post('crm-documents/media', 'CrmDocumentController@storeMedia')->name('crm-documents.storeMedia');
    Route::post('crm-documents/ckmedia', 'CrmDocumentController@storeCKEditorImages')->name('crm-documents.storeCKEditorImages');
    Route::resource('crm-documents', 'CrmDocumentController');

    Route::get('system-calendar', 'SystemCalendarController@index')->name('systemCalendar');
    Route::get('global-search', 'GlobalSearchController@search')->name('globalSearch');
    Route::get('messenger', 'MessengerController@index')->name('messenger.index');
    Route::get('messenger/create', 'MessengerController@createTopic')->name('messenger.createTopic');
    Route::post('messenger', 'MessengerController@storeTopic')->name('messenger.storeTopic');
    Route::get('messenger/inbox', 'MessengerController@showInbox')->name('messenger.showInbox');
    Route::get('messenger/outbox', 'MessengerController@showOutbox')->name('messenger.showOutbox');
    Route::get('messenger/{topic}', 'MessengerController@showMessages')->name('messenger.showMessages');
    Route::delete('messenger/{topic}', 'MessengerController@destroyTopic')->name('messenger.destroyTopic');
    Route::post('messenger/{topic}/reply', 'MessengerController@replyToTopic')->name('messenger.reply');
    Route::get('messenger/{topic}/reply', 'MessengerController@showReply')->name('messenger.showReply');
});
