<?php

return [
    'date_format'               => 'Y-m-d',
    'time_format'               => 'H:i:s',
    'primary_language'          => 'en',
    'available_languages'       => [
        'en' => 'English',
        'de' => 'German',
        'ru' => 'Russian',
        'es' => 'Spanish',
        'it' => 'Italian',
        'ar' => 'Arabic',
        'fr' => 'French',
    ],
    'registration_default_role' => '2',
];
