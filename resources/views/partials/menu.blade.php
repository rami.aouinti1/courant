<aside class="main-sidebar sidebar-dark-primary elevation-4" style="min-height: 917px;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">{{ trans('panel.site_title') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li>
                    <select class="searchable-field form-control">

                    </select>
                </li>
                <li class="nav-item">
                    <a href="{{ route("admin.home") }}" class="nav-link">
                        <p>
                            <i class="fas fa-fw fa-tachometer-alt">

                            </i>
                            <span>{{ trans('global.dashboard') }}</span>
                        </p>
                    </a>
                </li>
                @can('user_management_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/permissions*') ? 'menu-open' : '' }} {{ request()->is('admin/roles*') ? 'menu-open' : '' }} {{ request()->is('admin/users*') ? 'menu-open' : '' }} {{ request()->is('admin/audit-logs*') ? 'menu-open' : '' }} {{ request()->is('admin/diploms*') ? 'menu-open' : '' }} {{ request()->is('admin/certificates*') ? 'menu-open' : '' }} {{ request()->is('admin/associations*') ? 'menu-open' : '' }} {{ request()->is('admin/ancien-partis*') ? 'menu-open' : '' }} {{ request()->is('admin/languages*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-users">

                            </i>
                            <p>
                                <span>{{ trans('cruds.userManagement.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('permission_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-unlock-alt">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.permission.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('role_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-briefcase">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.role.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('user_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-user">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.user.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('audit_log_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.audit-logs.index") }}" class="nav-link {{ request()->is('admin/audit-logs') || request()->is('admin/audit-logs/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-file-alt">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.auditLog.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('diplom_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.diploms.index") }}" class="nav-link {{ request()->is('admin/diploms') || request()->is('admin/diploms/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-cogs">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.diplom.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('certificate_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.certificates.index") }}" class="nav-link {{ request()->is('admin/certificates') || request()->is('admin/certificates/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-cogs">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.certificate.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('association_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.associations.index") }}" class="nav-link {{ request()->is('admin/associations') || request()->is('admin/associations/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-cogs">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.association.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('ancien_parti_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.ancien-partis.index") }}" class="nav-link {{ request()->is('admin/ancien-partis') || request()->is('admin/ancien-partis/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-cogs">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.ancienParti.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('language_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.languages.index") }}" class="nav-link {{ request()->is('admin/languages') || request()->is('admin/languages/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-globe">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.language.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('depatement_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/bureauxes*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-archway">

                            </i>
                            <p>
                                <span>{{ trans('cruds.depatement.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('bureaux_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.bureauxes.index") }}" class="nav-link {{ request()->is('admin/bureauxes') || request()->is('admin/bureauxes/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-cogs">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.bureaux.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('bureauregional_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/news*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-building">

                            </i>
                            <p>
                                <span>{{ trans('cruds.bureauregional.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('chat_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/chat-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-comment-alt">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.chat.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('chat_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.chat-tests.index") }}" class="nav-link {{ request()->is('admin/chat-tests') || request()->is('admin/chat-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.chatTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('blog_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/blog-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-th-large">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.blog.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('blog_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.blog-tests.index") }}" class="nav-link {{ request()->is('admin/blog-tests') || request()->is('admin/blog-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.blogTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('member_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/member-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-user-friends">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.member.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('member_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.member-tests.index") }}" class="nav-link {{ request()->is('admin/member-tests') || request()->is('admin/member-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.memberTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('bureau_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/bureau-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-audio-description">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.bureau.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('bureau_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.bureau-tests.index") }}" class="nav-link {{ request()->is('admin/bureau-tests') || request()->is('admin/bureau-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.bureauTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('rapport_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/rapport-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-file">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.rapport.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('rapport_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.rapport-tests.index") }}" class="nav-link {{ request()->is('admin/rapport-tests') || request()->is('admin/rapport-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.rapportTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('rapport_financier_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/rapportfinancier-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw far fa-file-excel">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.rapportFinancier.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('rapportfinancier_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.rapportfinancier-tests.index") }}" class="nav-link {{ request()->is('admin/rapportfinancier-tests') || request()->is('admin/rapportfinancier-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.rapportfinancierTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('vote_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/vote-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-hands-helping">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.vote.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('vote_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.vote-tests.index") }}" class="nav-link {{ request()->is('admin/vote-tests') || request()->is('admin/vote-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.voteTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('conference_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/conference-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-video">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.conference.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('conference_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.conference-tests.index") }}" class="nav-link {{ request()->is('admin/conference-tests') || request()->is('admin/conference-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.conferenceTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('gallery_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/gallery-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-image">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.gallery.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('gallery_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.gallery-tests.index") }}" class="nav-link {{ request()->is('admin/gallery-tests') || request()->is('admin/gallery-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.galleryTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('news_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.news.index") }}" class="nav-link {{ request()->is('admin/news') || request()->is('admin/news/*') ? 'active' : '' }}">
                                        <i class="fa-fw fab fa-hacker-news-square">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.news.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('task_management_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/task-statuses*') ? 'menu-open' : '' }} {{ request()->is('admin/task-tags*') ? 'menu-open' : '' }} {{ request()->is('admin/tasks*') ? 'menu-open' : '' }} {{ request()->is('admin/tasks-calendars*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-calendar">

                            </i>
                            <p>
                                <span>{{ trans('cruds.taskManagement.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('task_status_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.task-statuses.index") }}" class="nav-link {{ request()->is('admin/task-statuses') || request()->is('admin/task-statuses/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-server">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.taskStatus.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('task_tag_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.task-tags.index") }}" class="nav-link {{ request()->is('admin/task-tags') || request()->is('admin/task-tags/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-server">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.taskTag.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('task_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.tasks.index") }}" class="nav-link {{ request()->is('admin/tasks') || request()->is('admin/tasks/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-briefcase">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.task.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('tasks_calendar_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.tasks-calendars.index") }}" class="nav-link {{ request()->is('admin/tasks-calendars') || request()->is('admin/tasks-calendars/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-calendar">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.tasksCalendar.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('user_alert_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.user-alerts.index") }}" class="nav-link {{ request()->is('admin/user-alerts') || request()->is('admin/user-alerts/*') ? 'active' : '' }}">
                            <i class="fa-fw fas fa-bell">

                            </i>
                            <p>
                                <span>{{ trans('cruds.userAlert.title') }}</span>
                            </p>
                        </a>
                    </li>
                @endcan
                @can('library_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-book">

                            </i>
                            <p>
                                <span>{{ trans('cruds.library.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('courant_democrate_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/logos*') ? 'menu-open' : '' }} {{ request()->is('admin/la-ligne-politiques*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-bicycle">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.courantDemocrate.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('logo_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.logos.index") }}" class="nav-link {{ request()->is('admin/logos') || request()->is('admin/logos/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-image">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.logo.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                        @can('la_ligne_politique_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.la-ligne-politiques.index") }}" class="nav-link {{ request()->is('admin/la-ligne-politiques') || request()->is('admin/la-ligne-politiques/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.laLignePolitique.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('tunisium_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/the-constitutions*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-globe-americas">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.tunisium.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('the_constitution_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.the-constitutions.index") }}" class="nav-link {{ request()->is('admin/the-constitutions') || request()->is('admin/the-constitutions/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.theConstitution.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('other_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/videos*') ? 'menu-open' : '' }} {{ request()->is('admin/pdfs*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-cogs">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.other.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('video_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.videos.index") }}" class="nav-link {{ request()->is('admin/videos') || request()->is('admin/videos/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fab fa-youtube">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.video.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                        @can('pdf_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.pdfs.index") }}" class="nav-link {{ request()->is('admin/pdfs') || request()->is('admin/pdfs/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-file-pdf">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.pdf.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('statistic_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/statistices-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-signal">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.statistic.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('statistices_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.statistices-tests.index") }}" class="nav-link {{ request()->is('admin/statistices-tests') || request()->is('admin/statistices-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.statisticesTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('archive_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/archives-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-images">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.archive.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('archives_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.archives-tests.index") }}" class="nav-link {{ request()->is('admin/archives-tests') || request()->is('admin/archives-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.archivesTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('quiz_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/quiz-tests*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-question-circle">

                            </i>
                            <p>
                                <span>{{ trans('cruds.quiz.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('quiz_test_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.quiz-tests.index") }}" class="nav-link {{ request()->is('admin/quiz-tests') || request()->is('admin/quiz-tests/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-cogs">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.quizTest.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('depute_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/*') ? 'menu-open' : '' }} {{ request()->is('admin/*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-graduation-cap">

                            </i>
                            <p>
                                <span>{{ trans('cruds.depute.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('event_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/deputeseventes*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-cogs">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.event.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('deputesevente_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.deputeseventes.index") }}" class="nav-link {{ request()->is('admin/deputeseventes') || request()->is('admin/deputeseventes/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.deputesevente.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                            @can('dossier_access')
                                <li class="nav-item has-treeview {{ request()->is('admin/dossiers-tests*') ? 'menu-open' : '' }}">
                                    <a class="nav-link nav-dropdown-toggle" href="#">
                                        <i class="fa-fw fas fa-file-export">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.dossier.title') }}</span>
                                            <i class="right fa fa-fw fa-angle-left"></i>
                                        </p>
                                    </a>
                                    <ul class="nav nav-treeview">
                                        @can('dossiers_test_access')
                                            <li class="nav-item">
                                                <a href="{{ route("admin.dossiers-tests.index") }}" class="nav-link {{ request()->is('admin/dossiers-tests') || request()->is('admin/dossiers-tests/*') ? 'active' : '' }}">
                                                    <i class="fa-fw fas fa-cogs">

                                                    </i>
                                                    <p>
                                                        <span>{{ trans('cruds.dossiersTest.title') }}</span>
                                                    </p>
                                                </a>
                                            </li>
                                        @endcan
                                    </ul>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('documentation_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/documentation-tests*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-passport">

                            </i>
                            <p>
                                <span>{{ trans('cruds.documentation.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('documentation_test_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.documentation-tests.index") }}" class="nav-link {{ request()->is('admin/documentation-tests') || request()->is('admin/documentation-tests/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-cogs">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.documentationTest.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('country_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.countries.index") }}" class="nav-link {{ request()->is('admin/countries') || request()->is('admin/countries/*') ? 'active' : '' }}">
                            <i class="fa-fw fas fa-flag">

                            </i>
                            <p>
                                <span>{{ trans('cruds.country.title') }}</span>
                            </p>
                        </a>
                    </li>
                @endcan
                @can('basic_c_r_m_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/crm-statuses*') ? 'menu-open' : '' }} {{ request()->is('admin/crm-customers*') ? 'menu-open' : '' }} {{ request()->is('admin/crm-notes*') ? 'menu-open' : '' }} {{ request()->is('admin/crm-documents*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-briefcase">

                            </i>
                            <p>
                                <span>{{ trans('cruds.basicCRM.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('crm_status_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.crm-statuses.index") }}" class="nav-link {{ request()->is('admin/crm-statuses') || request()->is('admin/crm-statuses/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-folder">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.crmStatus.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('crm_customer_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.crm-customers.index") }}" class="nav-link {{ request()->is('admin/crm-customers') || request()->is('admin/crm-customers/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-user-plus">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.crmCustomer.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('crm_note_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.crm-notes.index") }}" class="nav-link {{ request()->is('admin/crm-notes') || request()->is('admin/crm-notes/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-sticky-note">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.crmNote.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('crm_document_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.crm-documents.index") }}" class="nav-link {{ request()->is('admin/crm-documents') || request()->is('admin/crm-documents/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-folder">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.crmDocument.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                <li class="nav-item">
                    <a href="{{ route("admin.systemCalendar") }}" class="nav-link {{ request()->is('admin/system-calendar') || request()->is('admin/system-calendar/*') ? 'active' : '' }}">
                        <i class="fas fa-fw fa-calendar">

                        </i>
                        <p>
                            <span>{{ trans('global.systemCalendar') }}</span>
                        </p>
                    </a>
                </li>
                @php($unread = \App\Model\Quiz\QaTopic::unreadCount())
                    <li class="nav-item">
                        <a href="{{ route("admin.messenger.index") }}" class="{{ request()->is('admin/messenger') || request()->is('admin/messenger/*') ? 'active' : '' }} nav-link">
                            <i class="fa-fw fa fa-envelope">

                            </i>
                            <span>{{ trans('global.messages') }}</span>
                            @if($unread > 0)
                                <strong>( {{ $unread }} )</strong>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                            <p>
                                <i class="fas fa-fw fa-sign-out-alt">

                                </i>
                                <span>{{ trans('global.logout') }}</span>
                            </p>
                        </a>
                    </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
