@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.deputesevente.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.deputeseventes.update", [$deputesevente->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="events">{{ trans('cruds.deputesevente.fields.events') }}</label>
                <input class="form-control {{ $errors->has('events') ? 'is-invalid' : '' }}" type="text" name="events" id="events" value="{{ old('events', $deputesevente->events) }}" required>
                @if($errors->has('events'))
                    <span class="text-danger">{{ $errors->first('events') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.deputesevente.fields.events_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection