<div class="m-3">
    @can('bureaux_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.bureauxes.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.bureaux.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            {{ trans('cruds.bureaux.title_singular') }} {{ trans('global.list') }}
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-Bureaux">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            <th>
                                {{ trans('cruds.bureaux.fields.id') }}
                            </th>
                            <th>
                                {{ trans('cruds.bureaux.fields.bureau') }}
                            </th>
                            <th>
                                {{ trans('cruds.bureaux.fields.state') }}
                            </th>
                            <th>
                                {{ trans('cruds.bureaux.fields.city') }}
                            </th>
                            <th>
                                {{ trans('cruds.bureaux.fields.date_beginn') }}
                            </th>
                            <th>
                                {{ trans('cruds.bureaux.fields.date_end') }}
                            </th>
                            <th>
                                {{ trans('cruds.bureaux.fields.active') }}
                            </th>
                            <th>
                                {{ trans('cruds.bureaux.fields.members') }}
                            </th>
                            <th>
                                {{ trans('cruds.bureaux.fields.country') }}
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bureauxes as $key => $bureaux)
                            <tr data-entry-id="{{ $bureaux->id }}">
                                <td>

                                </td>
                                <td>
                                    {{ $bureaux->id ?? '' }}
                                </td>
                                <td>
                                    {{ App\Bureaux::BUREAU_SELECT[$bureaux->bureau] ?? '' }}
                                </td>
                                <td>
                                    {{ $bureaux->state ?? '' }}
                                </td>
                                <td>
                                    {{ $bureaux->city ?? '' }}
                                </td>
                                <td>
                                    {{ $bureaux->date_beginn ?? '' }}
                                </td>
                                <td>
                                    {{ $bureaux->date_end ?? '' }}
                                </td>
                                <td>
                                    <span style="display:none">{{ $bureaux->active ?? '' }}</span>
                                    <input type="checkbox" disabled="disabled" {{ $bureaux->active ? 'checked' : '' }}>
                                </td>
                                <td>
                                    @foreach($bureaux->members as $key => $item)
                                        <span class="badge badge-info">{{ $item->name }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    {{ $bureaux->country->name ?? '' }}
                                </td>
                                <td>
                                    @can('bureaux_show')
                                        <a class="btn btn-xs btn-primary" href="{{ route('admin.bureauxes.show', $bureaux->id) }}">
                                            {{ trans('global.view') }}
                                        </a>
                                    @endcan

                                    @can('bureaux_edit')
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.bureauxes.edit', $bureaux->id) }}">
                                            {{ trans('global.edit') }}
                                        </a>
                                    @endcan

                                    @can('bureaux_delete')
                                        <form action="{{ route('admin.bureauxes.destroy', $bureaux->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        </form>
                                    @endcan

                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('bureaux_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.bureauxes.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Bureaux:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection