@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.rapportTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.rapport-tests.update", [$rapportTest->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="rapport">{{ trans('cruds.rapportTest.fields.rapport') }}</label>
                <input class="form-control {{ $errors->has('rapport') ? 'is-invalid' : '' }}" type="text" name="rapport" id="rapport" value="{{ old('rapport', $rapportTest->rapport) }}" required>
                @if($errors->has('rapport'))
                    <span class="text-danger">{{ $errors->first('rapport') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.rapportTest.fields.rapport_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection