@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.association.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.associations.update", [$association->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.association.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $association->name) }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.association.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="function">{{ trans('cruds.association.fields.function') }}</label>
                <textarea class="form-control {{ $errors->has('function') ? 'is-invalid' : '' }}" name="function" id="function">{{ old('function', $association->function) }}</textarea>
                @if($errors->has('function'))
                    <span class="text-danger">{{ $errors->first('function') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.association.fields.function_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="date_beginn">{{ trans('cruds.association.fields.date_beginn') }}</label>
                <input class="form-control date {{ $errors->has('date_beginn') ? 'is-invalid' : '' }}" type="text" name="date_beginn" id="date_beginn" value="{{ old('date_beginn', $association->date_beginn) }}" required>
                @if($errors->has('date_beginn'))
                    <span class="text-danger">{{ $errors->first('date_beginn') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.association.fields.date_beginn_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_end">{{ trans('cruds.association.fields.date_end') }}</label>
                <input class="form-control date {{ $errors->has('date_end') ? 'is-invalid' : '' }}" type="text" name="date_end" id="date_end" value="{{ old('date_end', $association->date_end) }}">
                @if($errors->has('date_end'))
                    <span class="text-danger">{{ $errors->first('date_end') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.association.fields.date_end_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="certification">{{ trans('cruds.association.fields.certification') }}</label>
                <div class="needsclick dropzone {{ $errors->has('certification') ? 'is-invalid' : '' }}" id="certification-dropzone">
                </div>
                @if($errors->has('certification'))
                    <span class="text-danger">{{ $errors->first('certification') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.association.fields.certification_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.association.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ ($association->user ? $association->user->id : old('user_id')) == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <span class="text-danger">{{ $errors->first('user') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.association.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    var uploadedCertificationMap = {}
Dropzone.options.certificationDropzone = {
    url: '{{ route('admin.associations.storeMedia') }}',
    maxFilesize: 10, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 10
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="certification[]" value="' + response.name + '">')
      uploadedCertificationMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedCertificationMap[file.name]
      }
      $('form').find('input[name="certification[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($association) && $association->certification)
          var files =
            {!! json_encode($association->certification) !!}
              for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="certification[]" value="' + file.file_name + '">')
            }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection