@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.association.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.associations.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.association.fields.id') }}
                        </th>
                        <td>
                            {{ $association->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.association.fields.name') }}
                        </th>
                        <td>
                            {{ $association->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.association.fields.function') }}
                        </th>
                        <td>
                            {{ $association->function }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.association.fields.date_beginn') }}
                        </th>
                        <td>
                            {{ $association->date_beginn }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.association.fields.date_end') }}
                        </th>
                        <td>
                            {{ $association->date_end }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.association.fields.certification') }}
                        </th>
                        <td>
                            @foreach($association->certification as $key => $media)
                                <a href="{{ $media->getUrl() }}" target="_blank">
                                    {{ trans('global.view_file') }}
                                </a>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.association.fields.user') }}
                        </th>
                        <td>
                            {{ $association->user->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.associations.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection