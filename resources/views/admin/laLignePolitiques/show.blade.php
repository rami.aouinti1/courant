@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.laLignePolitique.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.la-ligne-politiques.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.laLignePolitique.fields.id') }}
                        </th>
                        <td>
                            {{ $laLignePolitique->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.laLignePolitique.fields.title') }}
                        </th>
                        <td>
                            {{ $laLignePolitique->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.laLignePolitique.fields.content') }}
                        </th>
                        <td>
                            {!! $laLignePolitique->content !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.laLignePolitique.fields.date_creation') }}
                        </th>
                        <td>
                            {{ $laLignePolitique->date_creation }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.la-ligne-politiques.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection