@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.user.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.users.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <td>
                            {{ $user->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.civilite') }}
                        </th>
                        <td>
                            {{ App\User::CIVILITE_SELECT[$user->civilite] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.firstanme') }}
                        </th>
                        <td>
                            {{ $user->firstanme }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        <td>
                            {{ $user->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.date_naiss') }}
                        </th>
                        <td>
                            {{ $user->date_naiss }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.approved') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $user->approved ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.email') }}
                        </th>
                        <td>
                            {{ $user->email }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.email_verified_at') }}
                        </th>
                        <td>
                            {{ $user->email_verified_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th>
                        <td>
                            @foreach($user->roles as $key => $roles)
                                <span class="label label-info">{{ $roles->title }}</span>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.cin') }}
                        </th>
                        <td>
                            {{ $user->cin }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.tel') }}
                        </th>
                        <td>
                            {{ $user->tel }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.adresse') }}
                        </th>
                        <td>
                            {{ $user->adresse }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.ville') }}
                        </th>
                        <td>
                            {{ $user->ville }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.region') }}
                        </th>
                        <td>
                            {{ $user->region }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.country') }}
                        </th>
                        <td>
                            {{ $user->country->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.fonction') }}
                        </th>
                        <td>
                            {{ App\User::FONCTION_SELECT[$user->fonction] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.facebook') }}
                        </th>
                        <td>
                            {{ $user->facebook }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.agree') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $user->agree ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.date_adhesion') }}
                        </th>
                        <td>
                            {{ $user->date_adhesion }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.photo') }}
                        </th>
                        <td>
                            @if($user->photo)
                                <a href="{{ $user->photo->getUrl() }}" target="_blank">
                                    <img src="{{ $user->photo->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.date_demande') }}
                        </th>
                        <td>
                            {{ $user->date_demande }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.users.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#user_diploms" role="tab" data-toggle="tab">
                {{ trans('cruds.diplom.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#user_certificates" role="tab" data-toggle="tab">
                {{ trans('cruds.certificate.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#user_associations" role="tab" data-toggle="tab">
                {{ trans('cruds.association.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#user_ancien_partis" role="tab" data-toggle="tab">
                {{ trans('cruds.ancienParti.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#user_languages" role="tab" data-toggle="tab">
                {{ trans('cruds.language.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#author_news" role="tab" data-toggle="tab">
                {{ trans('cruds.news.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#user_user_alerts" role="tab" data-toggle="tab">
                {{ trans('cruds.userAlert.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#members_bureauxes" role="tab" data-toggle="tab">
                {{ trans('cruds.bureaux.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#members_tasks" role="tab" data-toggle="tab">
                {{ trans('cruds.task.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="user_diploms">
            @includeIf('admin.users.relationships.userDiploms', ['diploms' => $user->userDiploms])
        </div>
        <div class="tab-pane" role="tabpanel" id="user_certificates">
            @includeIf('admin.users.relationships.userCertificates', ['certificates' => $user->userCertificates])
        </div>
        <div class="tab-pane" role="tabpanel" id="user_associations">
            @includeIf('admin.users.relationships.userAssociations', ['associations' => $user->userAssociations])
        </div>
        <div class="tab-pane" role="tabpanel" id="user_ancien_partis">
            @includeIf('admin.users.relationships.userAncienPartis', ['ancienPartis' => $user->userAncienPartis])
        </div>
        <div class="tab-pane" role="tabpanel" id="user_languages">
            @includeIf('admin.users.relationships.userLanguages', ['languages' => $user->userLanguages])
        </div>
        <div class="tab-pane" role="tabpanel" id="author_news">
            @includeIf('admin.users.relationships.authorNews', ['news' => $user->authorNews])
        </div>
        <div class="tab-pane" role="tabpanel" id="user_user_alerts">
            @includeIf('admin.users.relationships.userUserAlerts', ['userAlerts' => $user->userUserAlerts])
        </div>
        <div class="tab-pane" role="tabpanel" id="members_bureauxes">
            @includeIf('admin.users.relationships.membersBureauxes', ['bureauxes' => $user->membersBureauxes])
        </div>
        <div class="tab-pane" role="tabpanel" id="members_tasks">
            @includeIf('admin.users.relationships.membersTasks', ['tasks' => $user->membersTasks])
        </div>
    </div>
</div>

@endsection