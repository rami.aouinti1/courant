@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.user.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.users.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required">{{ trans('cruds.user.fields.civilite') }}</label>
                <select class="form-control {{ $errors->has('civilite') ? 'is-invalid' : '' }}" name="civilite" id="civilite" required>
                    <option value disabled {{ old('civilite', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\User::CIVILITE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('civilite', 'Homme') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('civilite'))
                    <span class="text-danger">{{ $errors->first('civilite') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.civilite_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="firstanme">{{ trans('cruds.user.fields.firstanme') }}</label>
                <input class="form-control {{ $errors->has('firstanme') ? 'is-invalid' : '' }}" type="text" name="firstanme" id="firstanme" value="{{ old('firstanme', '') }}" required>
                @if($errors->has('firstanme'))
                    <span class="text-danger">{{ $errors->first('firstanme') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.firstanme_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.user.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_naiss">{{ trans('cruds.user.fields.date_naiss') }}</label>
                <input class="form-control date {{ $errors->has('date_naiss') ? 'is-invalid' : '' }}" type="text" name="date_naiss" id="date_naiss" value="{{ old('date_naiss') }}">
                @if($errors->has('date_naiss'))
                    <span class="text-danger">{{ $errors->first('date_naiss') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.date_naiss_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('approved') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="approved" value="0">
                    <input class="form-check-input" type="checkbox" name="approved" id="approved" value="1" {{ old('approved', 0) == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="approved">{{ trans('cruds.user.fields.approved') }}</label>
                </div>
                @if($errors->has('approved'))
                    <span class="text-danger">{{ $errors->first('approved') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.approved_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="email">{{ trans('cruds.user.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" id="email" value="{{ old('email') }}" required>
                @if($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="password">{{ trans('cruds.user.fields.password') }}</label>
                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" id="password" required>
                @if($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.password_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="roles">{{ trans('cruds.user.fields.roles') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('roles') ? 'is-invalid' : '' }}" name="roles[]" id="roles" multiple required>
                    @foreach($roles as $id => $roles)
                        <option value="{{ $id }}" {{ in_array($id, old('roles', [])) ? 'selected' : '' }}>{{ $roles }}</option>
                    @endforeach
                </select>
                @if($errors->has('roles'))
                    <span class="text-danger">{{ $errors->first('roles') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.roles_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="cin">{{ trans('cruds.user.fields.cin') }}</label>
                <input class="form-control {{ $errors->has('cin') ? 'is-invalid' : '' }}" type="text" name="cin" id="cin" value="{{ old('cin', '') }}">
                @if($errors->has('cin'))
                    <span class="text-danger">{{ $errors->first('cin') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.cin_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="tel">{{ trans('cruds.user.fields.tel') }}</label>
                <input class="form-control {{ $errors->has('tel') ? 'is-invalid' : '' }}" type="text" name="tel" id="tel" value="{{ old('tel', '') }}">
                @if($errors->has('tel'))
                    <span class="text-danger">{{ $errors->first('tel') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.tel_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="adresse">{{ trans('cruds.user.fields.adresse') }}</label>
                <input class="form-control {{ $errors->has('adresse') ? 'is-invalid' : '' }}" type="text" name="adresse" id="adresse" value="{{ old('adresse', '') }}">
                @if($errors->has('adresse'))
                    <span class="text-danger">{{ $errors->first('adresse') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.adresse_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="ville">{{ trans('cruds.user.fields.ville') }}</label>
                <input class="form-control {{ $errors->has('ville') ? 'is-invalid' : '' }}" type="text" name="ville" id="ville" value="{{ old('ville', '') }}">
                @if($errors->has('ville'))
                    <span class="text-danger">{{ $errors->first('ville') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.ville_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="region">{{ trans('cruds.user.fields.region') }}</label>
                <input class="form-control {{ $errors->has('region') ? 'is-invalid' : '' }}" type="text" name="region" id="region" value="{{ old('region', '') }}">
                @if($errors->has('region'))
                    <span class="text-danger">{{ $errors->first('region') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.region_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="country_id">{{ trans('cruds.user.fields.country') }}</label>
                <select class="form-control select2 {{ $errors->has('country') ? 'is-invalid' : '' }}" name="country_id" id="country_id" required>
                    @foreach($countries as $id => $country)
                        <option value="{{ $id }}" {{ old('country_id') == $id ? 'selected' : '' }}>{{ $country }}</option>
                    @endforeach
                </select>
                @if($errors->has('country'))
                    <span class="text-danger">{{ $errors->first('country') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.country_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('cruds.user.fields.fonction') }}</label>
                <select class="form-control {{ $errors->has('fonction') ? 'is-invalid' : '' }}" name="fonction" id="fonction">
                    <option value disabled {{ old('fonction', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\User::FONCTION_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('fonction', 'Student') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('fonction'))
                    <span class="text-danger">{{ $errors->first('fonction') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.fonction_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="facebook">{{ trans('cruds.user.fields.facebook') }}</label>
                <input class="form-control {{ $errors->has('facebook') ? 'is-invalid' : '' }}" type="text" name="facebook" id="facebook" value="{{ old('facebook', '') }}">
                @if($errors->has('facebook'))
                    <span class="text-danger">{{ $errors->first('facebook') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.facebook_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('agree') ? 'is-invalid' : '' }}">
                    <input class="form-check-input" type="checkbox" name="agree" id="agree" value="1" required {{ old('agree', 0) == 1 ? 'checked' : '' }}>
                    <label class="required form-check-label" for="agree">{{ trans('cruds.user.fields.agree') }}</label>
                </div>
                @if($errors->has('agree'))
                    <span class="text-danger">{{ $errors->first('agree') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.agree_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="date_adhesion">{{ trans('cruds.user.fields.date_adhesion') }}</label>
                <input class="form-control date {{ $errors->has('date_adhesion') ? 'is-invalid' : '' }}" type="text" name="date_adhesion" id="date_adhesion" value="{{ old('date_adhesion') }}" required>
                @if($errors->has('date_adhesion'))
                    <span class="text-danger">{{ $errors->first('date_adhesion') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.date_adhesion_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="photo">{{ trans('cruds.user.fields.photo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                </div>
                @if($errors->has('photo'))
                    <span class="text-danger">{{ $errors->first('photo') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.photo_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="date_demande">{{ trans('cruds.user.fields.date_demande') }}</label>
                <input class="form-control date {{ $errors->has('date_demande') ? 'is-invalid' : '' }}" type="text" name="date_demande" id="date_demande" value="{{ old('date_demande') }}" required>
                @if($errors->has('date_demande'))
                    <span class="text-danger">{{ $errors->first('date_demande') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.date_demande_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.photoDropzone = {
    url: '{{ route('admin.users.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="photo"]').remove()
      $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="photo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($user) && $user->photo)
      var file = {!! json_encode($user->photo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $user->photo->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection