<div class="m-3">
    @can('diplom_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.diploms.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.diplom.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            {{ trans('cruds.diplom.title_singular') }} {{ trans('global.list') }}
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-Diplom">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            <th>
                                {{ trans('cruds.diplom.fields.id') }}
                            </th>
                            <th>
                                {{ trans('cruds.diplom.fields.specialite') }}
                            </th>
                            <th>
                                {{ trans('cruds.diplom.fields.degree') }}
                            </th>
                            <th>
                                {{ trans('cruds.diplom.fields.ecole') }}
                            </th>
                            <th>
                                {{ trans('cruds.diplom.fields.date_beginn') }}
                            </th>
                            <th>
                                {{ trans('cruds.diplom.fields.date_end') }}
                            </th>
                            <th>
                                {{ trans('cruds.diplom.fields.files') }}
                            </th>
                            <th>
                                {{ trans('cruds.diplom.fields.user') }}
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($diploms as $key => $diplom)
                            <tr data-entry-id="{{ $diplom->id }}">
                                <td>

                                </td>
                                <td>
                                    {{ $diplom->id ?? '' }}
                                </td>
                                <td>
                                    {{ $diplom->specialite ?? '' }}
                                </td>
                                <td>
                                    {{ App\Diplom::DEGREE_SELECT[$diplom->degree] ?? '' }}
                                </td>
                                <td>
                                    {{ $diplom->ecole ?? '' }}
                                </td>
                                <td>
                                    {{ $diplom->date_beginn ?? '' }}
                                </td>
                                <td>
                                    {{ $diplom->date_end ?? '' }}
                                </td>
                                <td>
                                    @foreach($diplom->files as $key => $media)
                                        <a href="{{ $media->getUrl() }}" target="_blank">
                                            {{ trans('global.view_file') }}
                                        </a>
                                    @endforeach
                                </td>
                                <td>
                                    {{ $diplom->user->name ?? '' }}
                                </td>
                                <td>
                                    @can('diplom_show')
                                        <a class="btn btn-xs btn-primary" href="{{ route('admin.diploms.show', $diplom->id) }}">
                                            {{ trans('global.view') }}
                                        </a>
                                    @endcan

                                    @can('diplom_edit')
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.diploms.edit', $diplom->id) }}">
                                            {{ trans('global.edit') }}
                                        </a>
                                    @endcan

                                    @can('diplom_delete')
                                        <form action="{{ route('admin.diploms.destroy', $diplom->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        </form>
                                    @endcan

                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('diplom_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.diploms.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Diplom:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection