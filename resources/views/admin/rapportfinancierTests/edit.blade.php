@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.rapportfinancierTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.rapportfinancier-tests.update", [$rapportfinancierTest->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="rapport_financier">{{ trans('cruds.rapportfinancierTest.fields.rapport_financier') }}</label>
                <input class="form-control {{ $errors->has('rapport_financier') ? 'is-invalid' : '' }}" type="text" name="rapport_financier" id="rapport_financier" value="{{ old('rapport_financier', $rapportfinancierTest->rapport_financier) }}" required>
                @if($errors->has('rapport_financier'))
                    <span class="text-danger">{{ $errors->first('rapport_financier') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.rapportfinancierTest.fields.rapport_financier_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection