@extends('layouts.admin')
@section('content')
@can('rapportfinancier_test_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.rapportfinancier-tests.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.rapportfinancierTest.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.rapportfinancierTest.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-RapportfinancierTest">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.rapportfinancierTest.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.rapportfinancierTest.fields.rapport_financier') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rapportfinancierTests as $key => $rapportfinancierTest)
                        <tr data-entry-id="{{ $rapportfinancierTest->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $rapportfinancierTest->id ?? '' }}
                            </td>
                            <td>
                                {{ $rapportfinancierTest->rapport_financier ?? '' }}
                            </td>
                            <td>
                                @can('rapportfinancier_test_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.rapportfinancier-tests.show', $rapportfinancierTest->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('rapportfinancier_test_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.rapportfinancier-tests.edit', $rapportfinancierTest->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('rapportfinancier_test_delete')
                                    <form action="{{ route('admin.rapportfinancier-tests.destroy', $rapportfinancierTest->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('rapportfinancier_test_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.rapportfinancier-tests.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-RapportfinancierTest:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection