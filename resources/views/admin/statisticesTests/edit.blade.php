@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.statisticesTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.statistices-tests.update", [$statisticesTest->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="statistic">{{ trans('cruds.statisticesTest.fields.statistic') }}</label>
                <input class="form-control {{ $errors->has('statistic') ? 'is-invalid' : '' }}" type="text" name="statistic" id="statistic" value="{{ old('statistic', $statisticesTest->statistic) }}" required>
                @if($errors->has('statistic'))
                    <span class="text-danger">{{ $errors->first('statistic') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.statisticesTest.fields.statistic_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection