@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.documentationTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.documentation-tests.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="docuementation">{{ trans('cruds.documentationTest.fields.docuementation') }}</label>
                <input class="form-control {{ $errors->has('docuementation') ? 'is-invalid' : '' }}" type="text" name="docuementation" id="docuementation" value="{{ old('docuementation', '') }}" required>
                @if($errors->has('docuementation'))
                    <span class="text-danger">{{ $errors->first('docuementation') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.documentationTest.fields.docuementation_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection