@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.quizTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.quiz-tests.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="quiz">{{ trans('cruds.quizTest.fields.quiz') }}</label>
                <input class="form-control {{ $errors->has('quiz') ? 'is-invalid' : '' }}" type="text" name="quiz" id="quiz" value="{{ old('quiz', '') }}" required>
                @if($errors->has('quiz'))
                    <span class="text-danger">{{ $errors->first('quiz') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.quizTest.fields.quiz_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection