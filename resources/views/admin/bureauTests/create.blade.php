@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.bureauTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.bureau-tests.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="bureau">{{ trans('cruds.bureauTest.fields.bureau') }}</label>
                <input class="form-control {{ $errors->has('bureau') ? 'is-invalid' : '' }}" type="text" name="bureau" id="bureau" value="{{ old('bureau', '') }}" required>
                @if($errors->has('bureau'))
                    <span class="text-danger">{{ $errors->first('bureau') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.bureauTest.fields.bureau_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection