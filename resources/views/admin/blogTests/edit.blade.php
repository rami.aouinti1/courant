@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.blogTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.blog-tests.update", [$blogTest->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="blog">{{ trans('cruds.blogTest.fields.blog') }}</label>
                <input class="form-control {{ $errors->has('blog') ? 'is-invalid' : '' }}" type="text" name="blog" id="blog" value="{{ old('blog', $blogTest->blog) }}" required>
                @if($errors->has('blog'))
                    <span class="text-danger">{{ $errors->first('blog') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.blogTest.fields.blog_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection