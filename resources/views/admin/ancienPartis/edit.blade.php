@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.ancienParti.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.ancien-partis.update", [$ancienParti->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="parti">{{ trans('cruds.ancienParti.fields.parti') }}</label>
                <input class="form-control {{ $errors->has('parti') ? 'is-invalid' : '' }}" type="text" name="parti" id="parti" value="{{ old('parti', $ancienParti->parti) }}" required>
                @if($errors->has('parti'))
                    <span class="text-danger">{{ $errors->first('parti') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.ancienParti.fields.parti_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="date_beginn">{{ trans('cruds.ancienParti.fields.date_beginn') }}</label>
                <input class="form-control date {{ $errors->has('date_beginn') ? 'is-invalid' : '' }}" type="text" name="date_beginn" id="date_beginn" value="{{ old('date_beginn', $ancienParti->date_beginn) }}" required>
                @if($errors->has('date_beginn'))
                    <span class="text-danger">{{ $errors->first('date_beginn') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.ancienParti.fields.date_beginn_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="date_end">{{ trans('cruds.ancienParti.fields.date_end') }}</label>
                <input class="form-control date {{ $errors->has('date_end') ? 'is-invalid' : '' }}" type="text" name="date_end" id="date_end" value="{{ old('date_end', $ancienParti->date_end) }}" required>
                @if($errors->has('date_end'))
                    <span class="text-danger">{{ $errors->first('date_end') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.ancienParti.fields.date_end_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.ancienParti.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ ($ancienParti->user ? $ancienParti->user->id : old('user_id')) == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <span class="text-danger">{{ $errors->first('user') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.ancienParti.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection