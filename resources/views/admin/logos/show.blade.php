@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.logo.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.logos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.logo.fields.id') }}
                        </th>
                        <td>
                            {{ $logo->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.logo.fields.title') }}
                        </th>
                        <td>
                            {{ $logo->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.logo.fields.logo') }}
                        </th>
                        <td>
                            @if($logo->logo)
                                <a href="{{ $logo->logo->getUrl() }}" target="_blank">
                                    <img src="{{ $logo->logo->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.logo.fields.date_creation') }}
                        </th>
                        <td>
                            {{ $logo->date_creation }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.logos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection