@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.dossiersTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.dossiers-tests.update", [$dossiersTest->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="dossier">{{ trans('cruds.dossiersTest.fields.dossier') }}</label>
                <input class="form-control {{ $errors->has('dossier') ? 'is-invalid' : '' }}" type="text" name="dossier" id="dossier" value="{{ old('dossier', $dossiersTest->dossier) }}" required>
                @if($errors->has('dossier'))
                    <span class="text-danger">{{ $errors->first('dossier') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.dossiersTest.fields.dossier_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection