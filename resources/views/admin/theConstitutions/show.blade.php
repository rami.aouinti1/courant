@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.theConstitution.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.the-constitutions.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.theConstitution.fields.id') }}
                        </th>
                        <td>
                            {{ $theConstitution->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.theConstitution.fields.constitution') }}
                        </th>
                        <td>
                            {{ $theConstitution->constitution }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.theConstitution.fields.content') }}
                        </th>
                        <td>
                            {!! $theConstitution->content !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.theConstitution.fields.date') }}
                        </th>
                        <td>
                            {{ $theConstitution->date }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.the-constitutions.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection