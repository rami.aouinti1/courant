@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.archivesTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.archives-tests.update", [$archivesTest->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="archive">{{ trans('cruds.archivesTest.fields.archive') }}</label>
                <input class="form-control {{ $errors->has('archive') ? 'is-invalid' : '' }}" type="text" name="archive" id="archive" value="{{ old('archive', $archivesTest->archive) }}" required>
                @if($errors->has('archive'))
                    <span class="text-danger">{{ $errors->first('archive') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.archivesTest.fields.archive_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection