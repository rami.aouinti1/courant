@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.voteTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.vote-tests.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="vote">{{ trans('cruds.voteTest.fields.vote') }}</label>
                <input class="form-control {{ $errors->has('vote') ? 'is-invalid' : '' }}" type="text" name="vote" id="vote" value="{{ old('vote', '') }}" required>
                @if($errors->has('vote'))
                    <span class="text-danger">{{ $errors->first('vote') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.voteTest.fields.vote_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection