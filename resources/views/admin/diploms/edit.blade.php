@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.diplom.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.diploms.update", [$diplom->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="specialite">{{ trans('cruds.diplom.fields.specialite') }}</label>
                <input class="form-control {{ $errors->has('specialite') ? 'is-invalid' : '' }}" type="text" name="specialite" id="specialite" value="{{ old('specialite', $diplom->specialite) }}" required>
                @if($errors->has('specialite'))
                    <span class="text-danger">{{ $errors->first('specialite') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.diplom.fields.specialite_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.diplom.fields.degree') }}</label>
                <select class="form-control {{ $errors->has('degree') ? 'is-invalid' : '' }}" name="degree" id="degree" required>
                    <option value disabled {{ old('degree', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Diplom::DEGREE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('degree', $diplom->degree) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('degree'))
                    <span class="text-danger">{{ $errors->first('degree') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.diplom.fields.degree_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="ecole">{{ trans('cruds.diplom.fields.ecole') }}</label>
                <input class="form-control {{ $errors->has('ecole') ? 'is-invalid' : '' }}" type="text" name="ecole" id="ecole" value="{{ old('ecole', $diplom->ecole) }}" required>
                @if($errors->has('ecole'))
                    <span class="text-danger">{{ $errors->first('ecole') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.diplom.fields.ecole_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="date_beginn">{{ trans('cruds.diplom.fields.date_beginn') }}</label>
                <input class="form-control date {{ $errors->has('date_beginn') ? 'is-invalid' : '' }}" type="text" name="date_beginn" id="date_beginn" value="{{ old('date_beginn', $diplom->date_beginn) }}" required>
                @if($errors->has('date_beginn'))
                    <span class="text-danger">{{ $errors->first('date_beginn') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.diplom.fields.date_beginn_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_end">{{ trans('cruds.diplom.fields.date_end') }}</label>
                <input class="form-control date {{ $errors->has('date_end') ? 'is-invalid' : '' }}" type="text" name="date_end" id="date_end" value="{{ old('date_end', $diplom->date_end) }}">
                @if($errors->has('date_end'))
                    <span class="text-danger">{{ $errors->first('date_end') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.diplom.fields.date_end_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="files">{{ trans('cruds.diplom.fields.files') }}</label>
                <div class="needsclick dropzone {{ $errors->has('files') ? 'is-invalid' : '' }}" id="files-dropzone">
                </div>
                @if($errors->has('files'))
                    <span class="text-danger">{{ $errors->first('files') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.diplom.fields.files_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.diplom.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ ($diplom->user ? $diplom->user->id : old('user_id')) == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <span class="text-danger">{{ $errors->first('user') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.diplom.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    var uploadedFilesMap = {}
Dropzone.options.filesDropzone = {
    url: '{{ route('admin.diploms.storeMedia') }}',
    maxFilesize: 10, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 10
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="files[]" value="' + response.name + '">')
      uploadedFilesMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedFilesMap[file.name]
      }
      $('form').find('input[name="files[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($diplom) && $diplom->files)
          var files =
            {!! json_encode($diplom->files) !!}
              for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="files[]" value="' + file.file_name + '">')
            }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection