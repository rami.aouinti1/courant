@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.diplom.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.diploms.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.diplom.fields.id') }}
                        </th>
                        <td>
                            {{ $diplom->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.diplom.fields.specialite') }}
                        </th>
                        <td>
                            {{ $diplom->specialite }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.diplom.fields.degree') }}
                        </th>
                        <td>
                            {{ App\Diplom::DEGREE_SELECT[$diplom->degree] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.diplom.fields.ecole') }}
                        </th>
                        <td>
                            {{ $diplom->ecole }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.diplom.fields.date_beginn') }}
                        </th>
                        <td>
                            {{ $diplom->date_beginn }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.diplom.fields.date_end') }}
                        </th>
                        <td>
                            {{ $diplom->date_end }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.diplom.fields.files') }}
                        </th>
                        <td>
                            @foreach($diplom->files as $key => $media)
                                <a href="{{ $media->getUrl() }}" target="_blank">
                                    {{ trans('global.view_file') }}
                                </a>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.diplom.fields.user') }}
                        </th>
                        <td>
                            {{ $diplom->user->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.diploms.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection