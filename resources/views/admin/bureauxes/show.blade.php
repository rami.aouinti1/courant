@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.bureaux.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.bureauxes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.bureaux.fields.id') }}
                        </th>
                        <td>
                            {{ $bureaux->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bureaux.fields.bureau') }}
                        </th>
                        <td>
                            {{ App\Bureaux::BUREAU_SELECT[$bureaux->bureau] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bureaux.fields.state') }}
                        </th>
                        <td>
                            {{ $bureaux->state }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bureaux.fields.city') }}
                        </th>
                        <td>
                            {{ $bureaux->city }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bureaux.fields.date_beginn') }}
                        </th>
                        <td>
                            {{ $bureaux->date_beginn }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bureaux.fields.date_end') }}
                        </th>
                        <td>
                            {{ $bureaux->date_end }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bureaux.fields.active') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $bureaux->active ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bureaux.fields.members') }}
                        </th>
                        <td>
                            @foreach($bureaux->members as $key => $members)
                                <span class="label label-info">{{ $members->name }}</span>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.bureaux.fields.country') }}
                        </th>
                        <td>
                            {{ $bureaux->country->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.bureauxes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#bureau_tasks" role="tab" data-toggle="tab">
                {{ trans('cruds.task.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#group_ref_news" role="tab" data-toggle="tab">
                {{ trans('cruds.news.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="bureau_tasks">
            @includeIf('admin.bureauxes.relationships.bureauTasks', ['tasks' => $bureaux->bureauTasks])
        </div>
        <div class="tab-pane" role="tabpanel" id="group_ref_news">
            @includeIf('admin.bureauxes.relationships.groupRefNews', ['news' => $bureaux->groupRefNews])
        </div>
    </div>
</div>

@endsection