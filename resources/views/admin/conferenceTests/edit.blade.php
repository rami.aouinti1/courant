@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.conferenceTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.conference-tests.update", [$conferenceTest->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="conference">{{ trans('cruds.conferenceTest.fields.conference') }}</label>
                <input class="form-control {{ $errors->has('conference') ? 'is-invalid' : '' }}" type="text" name="conference" id="conference" value="{{ old('conference', $conferenceTest->conference) }}" required>
                @if($errors->has('conference'))
                    <span class="text-danger">{{ $errors->first('conference') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.conferenceTest.fields.conference_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection