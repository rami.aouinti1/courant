@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.memberTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.member-tests.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="member">{{ trans('cruds.memberTest.fields.member') }}</label>
                <input class="form-control {{ $errors->has('member') ? 'is-invalid' : '' }}" type="text" name="member" id="member" value="{{ old('member', '') }}" required>
                @if($errors->has('member'))
                    <span class="text-danger">{{ $errors->first('member') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.memberTest.fields.member_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection