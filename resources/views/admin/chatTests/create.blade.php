@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.chatTest.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.chat-tests.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="chat">{{ trans('cruds.chatTest.fields.chat') }}</label>
                <input class="form-control {{ $errors->has('chat') ? 'is-invalid' : '' }}" type="text" name="chat" id="chat" value="{{ old('chat', '') }}" required>
                @if($errors->has('chat'))
                    <span class="text-danger">{{ $errors->first('chat') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.chatTest.fields.chat_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection